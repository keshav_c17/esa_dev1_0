#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: main.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Date    : 26-10-2021 04:30 PM
# @Updated : 10-01-2022
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This is the main script that compiles all the sections together and generates the final report docx file."""
# '''===================================================================================================================

import os
import sys
import logging
from datetime import datetime

# Error Logging Config
log_filename = datetime.now().strftime('ESA_log_%d-%m-%Y_%H-%M-%S.txt')
if not os.path.exists("ESA_logs"):
    os.mkdir("ESA_logs")
logging.basicConfig(filename=f"ESA_logs\\{log_filename}", level=logging.DEBUG,
                    format='%(asctime)s' "%(message)s",
                    datefmt="%d-%m-%Y_%I-%M-%S-%p")

# Adding necessary folders to system's path variable
sys.path.insert(0, rf"{os.getcwd()}\Utilities")
cwd = os.getcwd().split("\\")
cwd.pop()
sys.path.append(r"{}".format("\\".join(cwd)))

# Modules Import
try:
    print("\nImporting required modules..", end="\n\n")
    from Utilities.esa_import_modules import *
except (ImportError, ModuleNotFoundError):
    print("Importing FAILED! Stopping further execution..\n")
    logging.exception("\n\nError while importing required modules\n\n")
    sys.exit()

# The front page of the report
try:
    fill_templates()
except PermissionError:
    logging.shutdown()
    os.remove(f"ESA_logs\\{log_filename}")
    sys.exit("ERROR: Please close the last generated report before generating any new one or "
             "provide a new report name as argument.\n")

doc_file = Document(os.getcwd() + f"\\ESA_[{sys.argv[2]}].docx")

figure_count, table_count = 0, 0       # Global variable for figure and table count respectively


def initialize_doc():
    try:
        print("Initializing the document file.")
        define_text_styles(doc_file)  # Defining all the necessary text styles
        main_section = doc_file.add_section(WD_SECTION_START.NEW_PAGE)  # Creating new section for 1.0 Executive_Summary
        create_page_layout(doc_file)  # Setting page layout
        add_header(main_section)      # HEADER
        add_footer(main_section)      # FOOTER
        print("Initialization complete.", end="\n\n")
        return 0
    except (Exception,):
        logging.exception("\n\nError in Initializing the document\n\n")
        print("\nDocument Initialization FAILED!\n")
        return -1


def create_section_1():
    try:
        print("Compiling Section 1..")
        section_1_heading(doc_file)    # ADDING HEADING TEXT
        creating_top_table(doc_file)   # TOP TABLE
        adding_graph_images(doc_file)  # ADDING GRAPHS
        create_align_table(doc_file)   # PIE GRAPH & SIDE TABLE
        sec_1_last_table(doc_file)     # LAST TABLE OF DEFECT IMAGES
        print("Section-1 created.", end="\n\n")
        return 0
    except (Exception,):
        logging.exception("\n\nError in SECTION-1\n\n")
        print("\nSECTION-1 FAILED!\n")
        return -1


def create_all_ToCs():
    try:
        print("Creating table of contents..", end="\n\n")
        add_main_toc(doc_file)    # Main Table of Content
        add_figure_toc(doc_file)  # List of Figures/Photographs
        add_videos_toc(doc_file)  # List of Videos
        add_table_toc(doc_file)   # List of Tables
        return 0
    except (Exception,):
        return -1


def create_section_2():
    try:
        print("Compiling Section-2..")
        section_2_heading(doc_file)
        sec_2_1_table(doc_file)
        sec_2_2_table(doc_file)
        sec_2_3_table(doc_file)
        print("Section-2 created.", end="\n\n")
        return 0
    except (Exception,):
        logging.exception("\n\nError in SECTION-2\n\n")
        print("\nSECTION - 2 FAILED!\n")
        return -1


def create_section_3():
    try:
        print("Compiling Section-3..")
        section_3_heading(doc_file)
        global figure_count, table_count
        figure_count, table_count = list(sec3_driver_func(doc_file).values())
        print("Section-3 created.", end="\n\n")
        return 0
    except(Exception,):
        logging.exception("\n\nError in SECTION-3\n\n")
        print("\nSECTION - 3 FAILED!\n")
        return -1


def create_section_4():
    try:
        print("Compiling Section-4..")
        section_4_heading(doc_file)
        heading_4_1(doc_file)
        global figure_count, table_count
        figure_count, table_count = heading_4_2(doc_file, figure_count), copy_spec_table(doc_file, table_count)
        heading_4_2_1(doc_file)
        heading_4_2_2(doc_file)
        heading_4_2_3(doc_file)
        table_count = table_4_3(doc_file, table_count)
        table_count = table_4_4(doc_file, table_count)
        table_count = weather_table(doc_file, table_count)
        print("Section-4 created.", end="\n\n")
        return 0
    except(Exception,):
        logging.exception("\n\nError in SECTION-4\n\n")
        print("\nSECTION - 4 FAILED!\n")
        return -1


def create_section_5():
    try:
        print("Compiling Section-5..")
        section_5_heading(doc_file)
        section_5_sub_heading(doc_file)
        sec5_driver_func(doc_file, figure_count)
        print("Section-5 created.", end="\n\n")
        return 0
    except(Exception,):
        logging.exception("\n\nError in SECTION-5\n\n")
        print("\nSECTION - 5 FAILED!\n")
        return -1


def create_section_6():
    try:
        print("Compiling Section-6..")
        section_6_heading(doc_file)
        sec6_driver_func(doc_file, table_count)
        print("Section-6 created.", end="\n\n")
        return 0
    except(Exception,):
        logging.exception("\n\nError in SECTION-6\n\n")
        print("\nSECTION - 6 FAILED!\n")
        return -1


def main():
    # Calling all the functions in sequence and storing the return values in a list
    return_values = [
        initialize_doc(),    # Create document with generic settings (e.g. headers, footers, margin, etc)
        create_section_1(),  # Section_1.0_[Executive Summary]
        create_all_ToCs(),   # Table of Content [main, figures, tables, videos]
        create_section_2(),  # Section_2.0_[Introduction]
        create_section_3(),  # Section_3.0_[Inspection Details]
        create_section_4(),  # Section_4.0_[Appendix]
        create_section_5(),  # Section_5.0_[List of Photographs]
        create_section_6()   # Section_6.0_[List of Videos]
    ]
    if len(set(return_values)) == 1 and set(return_values).pop() == 0:
        return True
    else:
        return False

if __name__ == "__main__":
    # Defining the command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("path", help="Enter path to the report's directory.")
    parser.add_argument("report_name",
                        help="Enter the location name for the report, e.g. Ennore, Udayavara, Solapur, etc.")
    parser.parse_args()

    bool_msg = main()

    # SAVING THE DOCX FILE, UPDATING TOC, OPENING THE DOCX FILE
    doc_file.save(f"ESA_[{sys.argv[2]}].docx")

    print("Updating table of contents..")
    update_toc(f"ESA_[{sys.argv[2]}].docx")
    print("TOCs Updated.", end="\n\n")

    if bool_msg:
        logging.shutdown()
        os.remove(f"ESA_logs\\{log_filename}")
        print("SUCCESS!!\n")
        os.system(f"start ESA_[{sys.argv[2]}].docx")
        print(f"Report for {sys.argv[2]} Generated Successfully.\n")
    else:
        print("FAILED!!\n")
        print(f"FAILED report is generated. Please check the log file for errors. \n")
