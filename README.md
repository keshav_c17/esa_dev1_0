# Executive Summary Automation #

### Brief ###
This program is designed for the automation of the Executive Summary report.
It reads data from the Excel sheet and images from the directory and generates
a Word document in .docx format.

### Minimum System Requirements:###
Program is tested on the system with configuration as mentioned below:

  * Python 3.10.0 [min. version required: Python 3.3+]
  * OS    : Windows 10*
  * CPU   : Intel i3-6006U @ 2.00 GHz 64-Bit
  * Memory: 8 GB RAM, 500 GB HDD

**This program is also tested on Ubuntu 20.04 LTS.*

### How to use it? ###

This program is delivered in a form of a binary file (.exe).  
*For example purpose assuming the binary file name as* `ESA.exe`  

*It is suggested to create a seperate new folder and paste the binary file and then start with the report generation.*  
*The application will generate the output report and a log file (if any error occurs) wherever the binary (.exe) file is placed.* 

Here are the important points on how to use the binary .exe file to generate the reports:  
  
1. Open Command Prompt or Windows PowerShell at the location where `ESA.exe` file is placed.  
   `Your can simply Shift+Right Click inside the folder where ESA.exe is placed and choose option "Open Command Prompt/PowerShell window here"`  
   .  
2. Syntax to run the .exe file:  
    `./ESA.exe <path_of_report_folder> <name_of_report>`  
   .
3. *Argument 1 : Path of the report folder. *  
   `E.g.:  C:\Users\Keshav C\Pictures\Report_Ennore`  
      
      *Wherever there is space in the path it should be quoted (" ").*  
      `E.g.: C:\Users\"Keshav C"\Pictures\Report_Ennore`  
  .  
  
4. *Argument 2: Name of the report. (Ennore, Udayavara, Bridge12, etc.)*  
    `E.g.: Ennore` or `Udayavara` or `Random123`  
    
    This argument is quite straightforward. You can give any name of your liking but Report name is prefered input  
    to make output filename more meaningful. It is not case sensitive and alpha-numeric name can be provided.  
    
    *Just make sure you dont give any spaces in the name or it will be read as Argument 3. And dont use any special characters*  
  . 
  
5. Final Command looks like:  
    `./ESA.exe C:\Users\"Keshav C"\Pictures\Report_Ennore Ennore`

Ensure following points for error free run:

* Report folder path should be correctly provided.
* Report folder should contain ESA specific folders like graph, Heatmaps, Scour, Defects, Gallery. (Scour is optional.)
* All the images and folder name should follow proper nomenclature.
* Ensure no image is missing in the directory.
* Excel sheet must have all the required information without any human error.

### Error Handling and Error Logging  
  
*  Whenever some error occurs in a certain section, a failed message will be displayed in the console for each that failed section   
   but the detail of the error will not be displayed rather it will be stored in a seprate log file.  
*  Log file is automatically generated when the application runs and gets deleted if no error is occured. 
*  Failed behaviour and messages will be like:  
    `SECTION - 5 FAILED!`
     
     `FAILED!!`  
     `Failed report of <name_of_report> is generated.`  
    ` Please check the log file for the errors.`  
  
*  Program will not stop when some error occurs in a section rather it will generate a failed report. Incase of crticial errors only program will
   stop right at that point.
  
  
### Current Release Note ###
>*[Current features added or current functionalities are mentioned here.]*

* ESA v1.0 under development


### Future Scope ###

* Complete support for Linux. [Broken TOCs]