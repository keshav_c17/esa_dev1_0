import os
import win32com.client
import inspect


def update_toc(file_name):
    """
    This code updates the table of content automatically.
    Code file should be placed in same directory as the final generated docx file.
    Original Source: https://stackoverflow.com/a/34818909
    Used Source: https://github.com/python-openxml/python-docx/issues/36#issuecomment-656127078
    """
    script_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
    file_path = os.path.join(script_dir, file_name)
    word = win32com.client.DispatchEx("Word.Application")
    doc = word.Documents.Open(file_path)
    doc.TablesOfContents(1).Update()
    doc.Close(SaveChanges=True)
    word.Quit()

