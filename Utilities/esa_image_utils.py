#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_image_utils.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This module gets the path of all the image files stored in a directory, also it provides the name of the piers
    in sorted manner when asked."""
# '''===================================================================================================================

import os
import re
import sys
import pandas as pd
from Utilities.esa_utils import check_excel_file_path


def sorted_alphanumeric(data):
    """
    Sort the given iterable in the way that humans expect.
    refer: https://stackoverflow.com/a/2669120
    refer2: http://www.codinghorror.com/blog/2007/12/sorting-for-humans-natural-sort-order.html
    """
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(data, key=alphanum_key)


excel_path = sys.argv[1]+"\\files\\Resources\\Automation_template.xlsx"
check_excel_file_path()


fig_path = sys.argv[1]+"\\files\\Resources\\Heatmaps"
defects_img_folder = sys.argv[1]+"\\files\\Resources\\Defects"
gallery_folder = sys.argv[1]+"\\files\\Resources\\Gallery"
graphs_dir = sys.argv[1]+"\\files\\Resources\\graph"
scour_dir = sys.argv[1]+"\\files\\Resources\\Scour"


def get_pier_path(pier_names=False, get_path=False):
    """
    ## [TODO] 1. Need of improvement in the logic, might break.
              2. Dependent on File name PIER_x
              3. Remove jpg/png dependency in code logic. [removed but optimization needed]
              4. All the paths are hard coded but needs to be dynamically pulled
                 to be functional on different systems.
    :param pier_names: True the return pier names list.
    :param get_path: True then return dir path of each pier list.
    :return: List accordingly.
    """

    # Changing extension of all the file from png to jpg if png exist [just for ease]
    for filename in os.listdir(fig_path):
        if filename[0] == "P" or "V" and filename[-4:] == ".png":
            in_file_name = os.path.join(fig_path, filename)
            if not os.path.isfile(in_file_name):
                continue
            os.path.splitext(filename)
            new_name = in_file_name.replace('.png', '.jpg')
            os.rename(in_file_name, new_name)

    file_name_ls = sorted_alphanumeric(os.listdir(fig_path))
    filter_names = []
    path_dict = {}
    for file in file_name_ls:
        if "section" in file:
            continue
        if file[0] == "P":
            if "_D." not in file:
                if ".jpg" in file:
                    attached_D = file[:file.find(".jpg")] + "_D" + file[file.find(".jpg"):]
                    if attached_D in file_name_ls:
                        path_dict[os.path.join(fig_path, file)] = os.path.join(fig_path, attached_D)
                    else:
                        path_dict[os.path.join(fig_path, file)] = None
                    filter_names.append(f"{file[:-4].split('_')[0].title()} {file[:-4].split('_')[1]}")

    if pier_names:
        return filter_names
    if get_path:
        return path_dict


def get_defects_img(img_name: str):
    """
    returns the path of defect image by getting defect ID [e.g. O1, O2, O3, etc] as parameter.
    :param img_name: parameter will be dynamic in section_3 table logic. getting directly from excel sheet
    :return: path
    """
    path = os.path.join(defects_img_folder, img_name)
    return path


def get_gallery_img(img_name=False, img_path=False):
    """
    TODO: path of excel sheet needed to be dynamically given or something.
    :param img_name: name
    :param img_path: path
    :return: name list or image paths as asked for.
    """
    img_name_ls = []
    df = pd.read_excel(excel_path, sheet_name="gallery")
    paths = []
    for name in df["Image_file_name"]:
        if name.startswith(" ") or name.endswith(" "):
            name = name.strip()
        if name+".jpg" in os.listdir(gallery_folder):
            paths.append(os.path.join(gallery_folder, name+".jpg"))
        elif name+".png" in os.listdir(gallery_folder):
            paths.append(os.path.join(gallery_folder, name+".png"))
        else:
            raise Exception("Image not found/Image name mismatch."
                            "\n1. Compare gallery images name in Excel with Directory images name."
                            "\n2. Check if all images are present."
                            "\n3. Check image extension. (.jpg, .png supported)")

        img_name_ls.append(" ".join(name.split("_")).title())

    if img_name:
        return img_name_ls
    if img_path:
        return paths


def get_graph_paths():
    graph_paths = []
    for filename in os.listdir(graphs_dir):
        graph_paths.append(os.path.join(graphs_dir, filename))

    return graph_paths


four_major = []


def get_4_major_defects(orange=False, yellow=False):
    try:
        if orange:
            required_color = "orange"
        elif yellow:
            required_color = "yellow"
        else:
            required_color = "red"

        i = 1
        while len(four_major) < 4:

            df = pd.read_excel(excel_path, sheet_name=f"defect_map_page_{i}")
            df = df[["Color_of_marker", "Defect_ID"]]
            defect_color_ls = list(df["Color_of_marker"])
            defect_id_ls = list(df["Defect_ID"])

            for idx, color in enumerate(defect_color_ls):
                if required_color not in defect_color_ls:
                    break
                if color == required_color:
                    # idx = defect_color_ls.index(color)  # will only get the first instance
                    defect_dir_ls = os.listdir(defects_img_folder)
                    defect_file = defect_id_ls[idx]
                    if defect_file+".jpg" in defect_dir_ls:
                        path = os.path.join(defects_img_folder, defect_file+".jpg")
                        four_major.append(path)
                    elif defect_file+".JPG" in defect_dir_ls:
                        path = os.path.join(defects_img_folder, defect_file+".JPG")
                        four_major.append(path)
                    elif defect_file+".jpeg" in defect_dir_ls:
                        path = os.path.join(defects_img_folder, defect_file+".jpeg")
                        four_major.append(path)
                    elif defect_file+".png" in defect_dir_ls:
                        path = os.path.join(defects_img_folder, defect_file+".png")
                        four_major.append(path)
                if len(four_major) == 4:
                    break
            i += 1

    except ValueError:
        try:
            get_4_major_defects(orange=True)
        except ValueError:
            get_4_major_defects(yellow=True)

    return four_major


def icons_path():
    if getattr(sys, "frozen", False):
        # If the 'frozen' flag is set, we are in bundled-app mode!
        resources_dir = sys._MEIPASS+"\\Resources"
    else:
        # Normal development mode. Use os.getcwd() or __file__ as appropriate in your case...
        cwd = os.getcwd()
        cwd_split = cwd.split("\\")
        cwd_split.append("Resources")
        resources_dir = "\\".join(cwd_split)

    top_icons_dir = resources_dir+"\\icons"
    top_icons = []
    for icon_name in os.listdir(top_icons_dir):
        top_icons.append(os.path.join(top_icons_dir, icon_name))

    defect_color_icons_dir = resources_dir+"\\defect_color_icons"
    defect_color_icons = []
    for icon_name in os.listdir(defect_color_icons_dir):
        defect_color_icons.append(os.path.join(defect_color_icons_dir, icon_name))

    struct_icons_dir = resources_dir+"\\structure_icons"
    struct_icons = []
    for icon_name in os.listdir(struct_icons_dir):
        struct_icons.append(os.path.join(struct_icons_dir, icon_name))

    return top_icons, defect_color_icons, struct_icons


def get_scour_img(pier_num, ov=False, dv=False):
    img_ext = os.listdir(scour_dir)[0].split(".")[-1]
    if ov:
        return os.path.join(scour_dir, f"ov_sc_PIER_{pier_num}"+f".{img_ext}")
    elif dv:
        return os.path.join(scour_dir, f"dv_sc_PIER_{pier_num}"+f".{img_ext}")
