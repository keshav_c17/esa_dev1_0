#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_section4.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This module creates the section 4 of the report."""
# '''===================================================================================================================

import sys
import os
import pandas as pd
from copy import deepcopy
from docx import Document
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT, WD_LINE_SPACING
from docx.enum.table import WD_CELL_VERTICAL_ALIGNMENT, WD_TABLE_ALIGNMENT, WD_TABLE_DIRECTION
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
from Utilities.esa_utils import add_section_heading, add_section_sub_heading, heading_font_change, check_excel_file_path
from Utilities.esa_utils import add_table_below_label, add_fig_below_label, add_section_ss_heading
from docx.shared import Pt, Cm, RGBColor


excel_path = sys.argv[1]+"\\files\\Resources\\Automation_template.xlsx"
check_excel_file_path()

if getattr(sys, "frozen", False):
    # If the 'frozen' flag is set, we are in bundled-app mode!
    resources_dir = sys._MEIPASS + "\\Resources"
else:
    # Normal development mode. Use os.getcwd() or __file__ as appropriate in your case...
    cwd = os.getcwd()
    cwd_split = cwd.split("\\")
    cwd_split.append("Resources")
    resources_dir = "\\".join(cwd_split)

beluga_docx = resources_dir+"\\rov_docx\\Beluga\\beluga.docx"
mikros_docx = resources_dir+"\\rov_docx\\Mikros\\mikros.docx"
orca_docx = resources_dir+"\\rov_docx\\Orca\\orca.docx"

beluga_photo = resources_dir+"\\rov_docx\\Beluga\\beluga.png"
orca_photo = resources_dir+"\\rov_docx\\Orca\\orca.png"
mikros_photo = resources_dir+"\\rov_docx\\Mikros\\mikros.png"

df = pd.read_excel(excel_path, sheet_name="executive_summary")
get_rov_name = df["Equipment Used"].iloc[0]


def section_4_heading(doc_obj):
    para = add_section_heading(doc_obj, "4.0 Appendix")
    heading_font_change(para, "Georgia", Pt(20), True, 0, 0, 0)
    doc_obj.add_paragraph()


def heading_4_1(doc_obj):
    add_section_sub_heading(doc_obj, "4.1 Inspecting Agency")
    para = doc_obj.add_paragraph()
    para.paragraph_format.left_indent = Pt(36)
    para.paragraph_format.line_spacing_rule = WD_LINE_SPACING.ONE_POINT_FIVE
    para.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
    planys_bold = para.add_run("M/s Planys Technologies Pvt. Ltd.")
    font = planys_bold.font
    font.bold = True
    para.add_run(" (https://www.planystech.com), registered at Chennai (IN) is the inspecting agency deployed for"
                 " executing underwater inspection using Remotely Operated Robotic Vehicle (RORV).")


def heading_4_2(doc_obj, f_count):
    add_section_sub_heading(doc_obj, "4.2 Equipment Details")

    list1 = [f"1 x {get_rov_name}", "1 x Tether Management System", "1 x Petrol Electric Generator", "Command Module"]
    for line in list1:
        para = doc_obj.add_paragraph(line, style='List Bullet 2')
        para.paragraph_format.left_indent = Cm(2)
        if list1[-1] == line:
            para.paragraph_format.space_after = Pt(0)

    list2 = ["1 x LCD Screen", "1 x Control Station Processing Unit", "1 x Wireless Router",
             "1 x Remote Control Joystick", "1 x Laptop"]
    for line in list2:
        para = doc_obj.add_paragraph(line, style="List Bullet 3")
        para.paragraph_format.left_indent = Cm(3)

    para = doc_obj.add_paragraph()
    para.paragraph_format.left_indent = Pt(36)
    para.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
    para.paragraph_format.line_spacing_rule = WD_LINE_SPACING.ONE_POINT_FIVE
    para.add_run(f"Underwater inspection vehicle {get_rov_name} developed by the inspecting agency ")
    planys_bold = para.add_run("Planys Technologies ")
    font = planys_bold.font
    font.bold = True
    para.add_run("with the following specifications as mentioned in table below, was used to carry out the inspection.")

    para = doc_obj.add_paragraph()
    img_run = para.add_run()
    if "beluga" in get_rov_name.lower():
        img_run.add_picture(beluga_photo, height=Cm(7.99), width=Cm(15.92))
    elif "orca" in get_rov_name.lower():
        img_run.add_picture(orca_photo, height=Cm(7.99), width=Cm(15.92))
    elif "mikros" in get_rov_name.lower():
        img_run.add_picture(mikros_photo, height=Cm(7.99), width=Cm(15.92))
    else:
        raise FileNotFoundError(" In Section_4 required ROV photo not found. Or check for error in spelling of ROV in excel")

    add_fig_below_label(doc_obj, f"Figure {f_count}: Photograph of {get_rov_name}")

    doc_obj.add_page_break()
    return f_count+1


def replaceText(document, search, replace):
    """
    original source: https://stackoverflow.com/a/48715025
    """
    for table in document.tables:
        for row in table.rows:
            for paragraph in row.cells:
                if search in paragraph.text:
                    paragraph.text = replace


def copy_spec_table(doc_obj, t_count):
    """
    original source: https://stackoverflow.com/a/48715025
    """

    if "beluga" in get_rov_name.lower():
        rov_doc = Document(beluga_docx)
    elif "orca" in get_rov_name.lower():
        rov_doc = Document(orca_docx)
    elif "mikros" in get_rov_name.lower():
        rov_doc = Document(mikros_docx)
    else:
        raise FileNotFoundError("In Section_4 required ROV Docx not found. Or check for error in spelling of ROV in excel")

    template = rov_doc.tables[0]
    tbl = template._tbl
    # Here we do the copy of the table
    new_tbl = deepcopy(tbl)
    # Then we do the replacement
    replaceText(doc_obj, '<<VALUE_TO_FIND>>', 'New value')
    paragraph = doc_obj.add_paragraph()
    paragraph.paragraph_format.space_after = Pt(0)
    # After that, we add the previously copied table
    paragraph._p.addnext(new_tbl)

    add_table_below_label(doc_obj, f"Table {t_count}: Details of Inspection Vehicle")

    return t_count+1


def heading_4_2_1(doc_obj):
    add_section_ss_heading(doc_obj, "4.2.1 Command Module")
    para = doc_obj.add_paragraph(
        "The RORV pilot used a Command Module for controlling the vehicle. The module was equipped with an LCD screen "
        "which displayed a Graphical User Interface (GUI) for assisting the pilot. The GUI displayed live feed from "
        "various cameras on the RORV as well as relayed various system parameters such as orientation, depth and speed "
        "of the vehicle in different directions. This helped the pilot in efficiently controlling the vehicle.")
    para.paragraph_format.space_before = Pt(10)
    para.paragraph_format.line_spacing_rule = WD_LINE_SPACING.ONE_POINT_FIVE
    para.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY


def heading_4_2_2(doc_obj):
    add_section_ss_heading(doc_obj, "4.2.2 Pressure Sensor")
    if "beluga" or "orca" in get_rov_name.lower():
        para = doc_obj.add_paragraph(
            "High-accuracy silicon strain gauge pressure transducer from Omega Engineering, rated for 200 PSI of "
            f"absolute pressure, was used for depth measurements by {get_rov_name} "
            "least count of the depth sensor is 0.01m.")

    elif "mikros" in get_rov_name.lower():
        para = doc_obj.add_paragraph(
            "High-accuracy silicon strain gauge pressure transducer from Keller, rated for 450 PSI of relative pressure,"
            " was used for depth measurements by ROV Mikros. Full scale accuracy of the depth sensor is 0.01%.")

    else:
        raise FileNotFoundError
    para.paragraph_format.space_before = Pt(10)
    para.paragraph_format.space_after = Pt(0)
    para.paragraph_format.line_spacing_rule = WD_LINE_SPACING.ONE_POINT_FIVE
    para.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY
    empty_para = doc_obj.add_paragraph()
    empty_para.paragraph_format.line_spacing = Pt(0.06)
    empty_para.paragraph_format.space_before = Pt(0)
    empty_para.paragraph_format.space_after = Pt(0)


def heading_4_2_3(doc_obj):
    add_section_ss_heading(doc_obj, "4.2.3 Laser Ranging")
    para = doc_obj.add_paragraph(
        "Laser ranging device with 650nm diodes was used for measuring the distance between any two points on the "
        "structure with high accuracy.")
    para.paragraph_format.space_before = Pt(10)
    para.paragraph_format.line_spacing_rule = WD_LINE_SPACING.ONE_POINT_FIVE
    para.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY


def table_4_3(doc_obj, t_count):
    add_section_sub_heading(doc_obj, "4.3 Personnel")
    para = doc_obj.add_paragraph("The following personnel were involved in the operation: ")
    para.alignment = WD_PARAGRAPH_ALIGNMENT.JUSTIFY

    df = pd.read_excel(excel_path, sheet_name="appendix", usecols="B:E")
    total_rows = len(df["Name"].dropna())
    df = df.iloc[:total_rows]

    table_head = doc_obj.add_table(rows=1, cols=4, style="Medium Grid 3 Accent 1")
    row = table_head.rows[0]
    row.height = Cm(0.62)

    head_row_data = ["Date", "Location", "Name", "Responsibility"]
    for col in range(len(head_row_data)):
        cell = table_head.cell(0, col)
        cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
        # change color fill of a cell https://stackoverflow.com/a/43467445
        shading_elm_1 = parse_xml(r'<w:shd {} w:fill="538A86"/>'.format(nsdecls('w')))
        cell._tc.get_or_add_tcPr().append(shading_elm_1)

        para = cell.paragraphs[0]
        para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        para.add_run(head_row_data[col])

    for row in range(1, total_rows+1):
        table_head.add_row()
        for col in range(4):
            cell = table_head.cell(row, col)
            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            # change color fill of a cell https://stackoverflow.com/a/43467445
            shading_elm_1 = parse_xml(r'<w:shd {} w:fill="DBEFE9"/>'.format(nsdecls('w')))
            cell._tc.get_or_add_tcPr().append(shading_elm_1)

            para = cell.paragraphs[0]
            if str(df.iloc[row-1, col]) == "nan":
                pass
            else:
                para.add_run(str(df.iloc[row-1, col]))

            if row-1 == 0 and col == 0:
                para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
                run = para.runs[0]
                font = run.font
                font.bold = False
                font.color.rgb = RGBColor(0, 0, 0)
            else:
                para.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT

    # for col in table_head.columns[:2]:
    col0 = table_head.columns[0]
    c1 = col0.cells[1]
    for cell in col0.cells[2:]:
        c1.merge(cell)

    col1 = table_head.columns[1]
    c1 = col1.cells[1]
    for cell in col1.cells[2:]:
        c1.merge(cell)

    add_table_below_label(doc_obj, f"Table {t_count}: Personnel Details")

    if total_rows > 4:
        doc_obj.add_page_break()

    return t_count+1


def table_4_4(doc_obj, t_count):
    add_section_sub_heading(doc_obj, "4.4 Inspection Procedure / Level")

    df = pd.read_excel(excel_path, sheet_name="appendix", usecols="G:H")

    table = doc_obj.add_table(rows=12, cols=2, style="Medium Grid 3 Accent 1")
    table.allow_autofit = False
    table.alignment = WD_TABLE_ALIGNMENT.CENTER

    for row in range(12):
        select_row = table.rows[row]
        select_row.height = Cm(0.8)
        for col in range(2):
            cell = table.cell(row, col)

            # change color fill of a cell https://stackoverflow.com/a/43467445
            shading_elm_1 = parse_xml(r'<w:shd {} w:fill="DBEFE9"/>'.format(nsdecls('w')))
            cell._tc.get_or_add_tcPr().append(shading_elm_1)

            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
            run = para.add_run(str(df.iloc[row, col]))
            if row == 0 and col == 1:
                font = run.font
                font.bold = False
                font.color.rgb = RGBColor(0, 0, 0)
            if col == 0:
                cell.width = Cm(6.27)
                # change color fill of a cell https://stackoverflow.com/a/43467445
                shading_elm_1 = parse_xml(r'<w:shd {} w:fill="538A86"/>'.format(nsdecls('w')))
                cell._tc.get_or_add_tcPr().append(shading_elm_1)
                font = run.font
                font.bold = False
            else:
                cell.width = Cm(8.54)

    add_table_below_label(doc_obj, f"Table {t_count}: Operational Setup (Visual Inspection)")

    return t_count+1


def weather_table(doc_obj, t_count):
    add_section_sub_heading(doc_obj, "4.5 Weather Conditions")

    df = pd.read_excel(excel_path, sheet_name="appendix", usecols="G:H")
    total_days = int(df.iat[8, 1].split()[0])

    table = doc_obj.add_table(rows=total_days+1, cols=5, style="ESA_table_style_main")
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    header_row_data = [("Date", 2.74), ("Temperature", 3), ("Weather", 3.75),
                       ("Time of Operation", 4.75), ("Tidal range", 2.75)]

    for row_idx in range(total_days+1):
        col_idx = 0
        row = table.rows[row_idx]
        row.height = Cm(0.7)
        for char, width in header_row_data:
            cell = table.cell(row_idx, col_idx)
            cell.width = Cm(width)
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            if row_idx == 0:
                run = para.add_run(char)
                font = run.font
                font.bold = False
            if row_idx != 0 and col_idx == 0:
                # change color fill of a cell https://stackoverflow.com/a/43467445
                shading_elm_1 = parse_xml(r'<w:shd {} w:fill="DBEFE9"/>'.format(nsdecls('w')))
                cell._tc.get_or_add_tcPr().append(shading_elm_1)
                run = para.add_run("")
                font = run.font
                font.bold = False
                font.color.rgb = RGBColor(0, 0, 0)

            col_idx += 1

    add_table_below_label(doc_obj, f"Table {t_count}: Weather conditions during the survey")

    source_run = doc_obj.add_paragraph().add_run("Source: \n")
    font = source_run.font
    font.size = Pt(11)

    doc_obj.add_page_break()

    return t_count + 1
