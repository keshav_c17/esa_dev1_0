#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_section6.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This module creates the section 6 [LIST OF VIDEOS]of the report."""
# '''===================================================================================================================

from Utilities.esa_utils import merge_hor_cell, insertHR
from Utilities.esa_utils import add_table_below_label, check_excel_file_path
from docx.shared import Pt, Cm, RGBColor
from docx.enum.table import WD_CELL_VERTICAL_ALIGNMENT, WD_TABLE_ALIGNMENT
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
import pandas as pd
from openpyxl import load_workbook
import sys

path = sys.argv[1]+"\\files\\Resources\\Automation_template.xlsx"
check_excel_file_path()


def get_sheetnames(filepath):
    """
    Using openpyxl module reading all the sheetnames of the excel file without actually opening them.
    :param filepath: Path of excel file.
    :return: list of sheetnames.

    TODO: Path of excel file should be provided dynamically.

    """
    wb = load_workbook(filepath, read_only=True, keep_links=False)
    return wb.sheetnames


def section_6_heading(doc_obj):
    """
    Section heading text is created.
    """
    para = doc_obj.add_paragraph("6.0 List of Videos", style="Intense Quote")
    para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    para.paragraph_format.left_indent = Cm(0)
    para.paragraph_format.right_indent = Cm(0)
    insertHR(para)
    style = doc_obj.styles["Intense Quote"]
    font = style.font
    para.style.font.color.rgb = RGBColor(0, 0, 0)
    font.italic = False
    font.bold = True
    font.name = "Georgia"
    font.size = Pt(20)
    doc_obj.add_paragraph()


def sec_6_sub_heading(doc_obj, sub_count, pier):
    para = doc_obj.add_paragraph(f"6.{sub_count}   Dive details of the {pier}")
    para.paragraph_format.space_after = Pt(12)
    para.style = doc_obj.styles["Quote"]
    style = doc_obj.styles["Quote"]
    font = style.font
    font.italic = False
    font.name = "Georgia"
    font.size = Pt(16)


def total_piers():
    """
    Gets the last inspection_page_number needed for the iteration.
    :return: integer number.
    """
    first_defect_page_idx = get_sheetnames(path).index("defect_map_page_1")
    last_insp_page = get_sheetnames(path)[first_defect_page_idx-1]
    num = last_insp_page[-2:]

    if "_" in num:  # in case the number has underscore in it then remove it.
        remove_under = num.replace("_", "")
        return int(remove_under)
    else:
        return int(num)


def table_header_row(doc_obj, pier):
    """
    Creates the header row of the table.
    :param doc_obj: doc object
    :param pier: pier name
    :return: table
    """
    header_table = doc_obj.add_table(rows=2, cols=2, style="ESA_table_style_main")
    header_table.alignment = WD_TABLE_ALIGNMENT.CENTER
    header_table.allow_autofit = False
    merge_hor_cell(header_table, 0)
    data = [{f"{pier} Dive Details": 14.93, "": ""}, {"Dive No.": 2.16, "Dive Details": 12.78}]
    for i in range(2):
        for j in range(2):
            if i == 0 and j == 1:
                header_table.rows[0].height = Cm(0.8)
                continue
            key = list(data[i])[j]
            value = data[i][key]
            cell = header_table.cell(i, j)
            cell.width = Cm(value)
            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            para.add_run(key)
    return header_table


def sec6_driver_func(doc_obj, table_count):
    """
    main driver function of section 6
    TODO: 1. Table count should be according to the section 4 and should be dynamically picked from that section.
          2. Right now count is hard-coded as section 4 has four table as of now.
          3. Function can be split further more if needed.
    :param doc_obj: doc object.
    :param table_count: getting directly from section 3 right now.
    :return: none
    """
    count = 0
    sub_sec_count = 1
    t_count = table_count  # + 4  # +4 because skipping Section 4.0 APPENDIX containing 4 tables
    for pier_num in range(1, total_piers()+1):
        df = pd.read_excel(path, sheet_name=f"inspection_map_page_{pier_num}")
        pier_name = " ".join(df["Block_name"][0].title().split("_"))
        if "Visual" in pier_name:
            pier_name = pier_name.replace("Visual", "Pier")
        # print(pier_name)

        sec_6_sub_heading(doc_obj, sub_sec_count, pier_name)
        main_table = table_header_row(doc_obj, pier_name)
        row_count = 0
        
        for line in df["Description"]:    # each row loop starts
            ls = line.split(" ")
            ls.insert(1, "Visual")
            for element in ls:
                # if element == "of":
                #     ls.remove(element)
                if "_" in element:
                    removed_underscore = element.replace("_", " ")
                    ls.remove(element)
                    ls.append(removed_underscore)
            count += 1
            cell_run = ' '.join(ls)

            row = main_table.add_row()
            row_count += 1
            row.height = Cm(0.64)

            cell1 = row.cells[0]
            cell1.width = Cm(2.16)
            para1 = cell1.paragraphs[0]
            run = para1.add_run(str(count))
            run.font.bold = False
            para1.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

            cell2 = row.cells[1]
            cell2.width = Cm(12.78)
            cell2.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            para2 = cell2.paragraphs[0]
            para2.add_run(cell_run)

        add_table_below_label(doc_obj, f"Table {t_count}: Dive details of {pier_name}")
        sub_sec_count += 1
        t_count += 1

        if not sub_sec_count % 2 == 0 or row_count >= 12:
            if count % 21 == 0:
                pass
                """
                There can be 21 rows in a page combining both of the table, after that
                automatically next paragraph will be shifted to next page so no need of
                adding a page break in such condition.
                This condition is in Udayavara report: after displaying pier 3 & 4 next pier 5
                information automatically goes to next page.
                TODO: 1. Test this code as many time as possible.
                """
            else:
                doc_obj.add_page_break()

        doc_obj.add_paragraph()







