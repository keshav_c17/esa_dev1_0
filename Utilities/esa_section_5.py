#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_section5.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This module creates the section 5 of the report."""
# '''===================================================================================================================

from Utilities.esa_utils import add_section_heading, add_section_sub_heading, heading_font_change
from Utilities.esa_utils import add_fig_below_label
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.shared import Pt, Cm
from Utilities.esa_image_utils import get_gallery_img


def section_5_heading(doc_obj):
    """
    Creates the heading text of the section 5.
    """
    para = add_section_heading(doc_obj, "5.0 List of Photographs")
    heading_font_change(para, "Georgia", Pt(20), True, 0, 0, 0)
    doc_obj.add_paragraph()


def section_5_sub_heading(doc_obj):
    """
    Creates the sub heading text of the section 5.
    """
    para = add_section_sub_heading(doc_obj, "5.1 Photographs")
    para.paragraph_format.left_indent = Cm(1)
    para.paragraph_format.space_after = Pt(5)


def sec5_driver_func(doc_obj, f_count):
    """
    The driver function of section 5 that adds the image along with the label one-by-one.

    TODO: Right now image name is taken from the excel sheet, shouldn't be a problem but check for the paths.

    :param doc_obj:
    :param f_count: figure count.
    """
    doc_obj.add_paragraph()
    img_name_ls = get_gallery_img(img_name=True)

    fig_count = f_count  # + 2
    for img_name in img_name_ls:
        img_para = doc_obj.add_paragraph()
        img_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        img_para.add_run().add_picture(get_gallery_img(img_path=True)[img_name_ls.index(img_name)], height=Cm(6.77), width=Cm(12.05))
        add_fig_below_label(doc_obj, f"Figure {fig_count}: Image of {img_name}")
        fig_count += 1

    doc_obj.add_page_break()
