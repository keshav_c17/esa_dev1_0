#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_section_3.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This module creates the section 3 of the report."""
# '''===================================================================================================================

from Utilities.esa_utils import add_section_heading, heading_font_change, merge_hor_cell
from Utilities.esa_utils import add_section_sub_heading, add_table_below_label, add_fig_below_label, add_new_row, \
    change_orientation
from Utilities.esa_utils import defect_table, add_defect_colored, add_section_ss_heading, check_excel_file_path
from Utilities.esa_image_utils import get_pier_path, get_defects_img, get_scour_img
from docx.shared import Pt, Cm, RGBColor
from docx.enum.table import WD_CELL_VERTICAL_ALIGNMENT, WD_TABLE_ALIGNMENT
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
import pandas as pd
import math
from collections import Counter
import sys

excel_path = sys.argv[1] + "\\files\\Resources\\Automation_template.xlsx"
check_excel_file_path()


def check_scour_exist(pier, scour_table_1_data=False, scour_table_2_data=False, scour_table_3_data=False, check=False):
    """
    This function will check availability of scour details, returns data set for different scour_tables
    @param pier: pier name
    @param scour_table_1_data: if true return data set for scour_table_1
    @param scour_table_2_data: if true return data set for scour_table_2
    @param scour_table_3_data: if true return data set for scour_table_3
    @param check: if true then only check scour details exist or not without any calculations.
    @return: conditional
    """
    try:
        df = pd.read_excel(excel_path, sheet_name="scour_details", usecols="A:H")
        if check:
            if df.set_index("Pier").loc[pier].isnull().all():
                return False
            else:
                return True

        elif scour_table_1_data:
            df_scour_1 = df.iloc[:, 0:5].set_index("Pier")
            return df_scour_1.loc[pier].reset_index()

        elif scour_table_2_data:
            df_scour_2 = df[["Pier", "River Side", "Seaside"]].set_index("Pier")
            return df_scour_2.loc[pier].reset_index()

        elif scour_table_3_data:
            df_scour_3 = df[["Pier", "Mid Span Sounding"]].set_index("Pier")
            return df_scour_3.loc[pier].reset_index()

    except ValueError:
        return False


def section_3_heading(doc_obj):
    """
    Creates section top heading and adds red line below.
    """
    para = add_section_heading(doc_obj, "3.0 Inspection Details")
    heading_font_change(para, "Georgia", Pt(20), True, 0, 0, 0)
    doc_obj.add_paragraph()


def pier_headings(doc_obj, sub_head_no, ss_head_no, pier):
    """
    Section sub heading and sub-sub heading is created.
    :param doc_obj: doc object
    :param sub_head_no: sub_heading_number will be fed from sec3_driver_func()
    :param ss_head_no: sub_sub_heading_number will be fed from sec3_driver_func()
    :param pier: Pier name will be fed from sec3_driver_func()
    :return:
    """
    add_section_sub_heading(doc_obj, f"3.{sub_head_no} Details of {pier}")
    add_section_ss_heading(doc_obj, f"\t3.{sub_head_no}.{ss_head_no} Dive Details")
    doc_obj.add_paragraph()


def add_dive_fig(doc_obj, fig_count, paths, pier_ls, pier):
    """
    Adds the dive detail figure 1 of 2

    ##[TODO] : Check paths please. should be dynamic.

    :param doc_obj:  doc object
    :param fig_count: count of figure
    :param paths: path fed from sec3_driver_func()
    :param pier_ls: pier name list fed from sec3_driver_func()
    :param pier: particular pier fed from sec3_driver_func()
    :return:
    """
    img_para = doc_obj.add_paragraph()  # Adding para for fig1
    img_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    img_para.add_run().add_picture(list(paths)[pier_ls.index(pier)], height=Cm(10.36), width=Cm(12.43))
    add_fig_below_label(doc_obj, f"Figure {fig_count}: Heatmap of dive detail at {pier}")


def get_element_table_data(pier_name):
    df = pd.read_excel(excel_path, sheet_name="inspection_details", index_col=0).dropna()

    arr = list(df.columns)
    data = list()
    data.append(("Element Description", ""))
    data.append(("Element", pier_name))
    for col_name in arr:
        val = df.at[pier_name, col_name]
        if col_name == ("No. of Dives" or "No of Dives"):
            float_to_int = int(val)
            data.append((col_name, str(float_to_int) + " Dives"))
        elif col_name == "Perimeter" and "m" not in str(val):
            data.append((col_name, f'{str(val)} m'))
        else:
            data.append((col_name, str(val)))

    return data


def element_table(doc_obj, sub_head_no, ss_head_no, table_count, pier):
    """
    Creates Element description table.

    ##[TODO]: Make it dynamic once you have the data.

    :param doc_obj:
    :param sub_head_no: fed from sec3_driver_func()
    :param ss_head_no: fed from sec3_driver_func()
    :param table_count: fed from sec3_driver_func()
    :param pier: fed from sec3_driver_func()
    :return:
    """
    insp_ss_head = doc_obj.add_heading(f"3.{sub_head_no}.{ss_head_no} Element Description", 3)
    insp_ss_head.paragraph_format.space_after = Pt(10)
    element_tbl = doc_obj.add_table(rows=7, cols=2, style="ESA_table_style_main")
    element_tbl.width = Cm(16.53)
    merge_hor_cell(element_tbl, 0)  # merging first row cells

    data = get_element_table_data(pier_name=pier)
    for i in range(7):
        row = element_tbl.rows[i]
        row.height = Cm(0.84)
        for j in range(2):
            cell = element_tbl.cell(i, j)
            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            if j == 0 and i != 0:
                cell.width = Cm(5.38)
            para = cell.paragraphs[0]
            para.add_run(data[i][j])
            if data[i][j] == "":
                para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

    add_table_below_label(doc_obj, f"Table {table_count}: Element Description")
    table_count += 1


def observation_page(doc_obj, sub_head_no, ss_head_no, paths, pier_ls, pier, fig_count):
    """
    First changes the orientation to landscape then adds the observation image.

    TODO: Check for the path, must be dynamically added.

    :param doc_obj: doc object
    :param sub_head_no: sec3_driver_func()
    :param ss_head_no: sec3_driver_func()
    :param paths: sec3_driver_func()
    :param pier_ls: sec3_driver_func()
    :param pier: sec3_driver_func()
    :param fig_count: figure count from sec3_driver_func()
    :return:
    """
    change_orientation(doc_obj, landscape=1)

    # insp_ss_head = doc_obj.add_heading(f"3.{sub_head_no}.{3} Observations from the inspection", 3)
    add_section_ss_heading(doc_obj, f"3.{sub_head_no}.{ss_head_no} Observations from the inspection")
    doc_obj.add_paragraph()
    img_para = doc_obj.add_paragraph()  # Adding para for fig2
    img_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    img_para.add_run().add_picture(list(paths.values())[pier_ls.index(pier)], height=Cm(12.98), width=Cm(15.54))
    add_fig_below_label(doc_obj, f"Figure {fig_count}: Heatmap and observations at {pier}")


def get_defect_excel_data(defect_sheet_no):
    """
    READS THE EXCEL DEFECT PAGES SHEET BY SHEET.
    Need to provide particular defect sheet number.

    TODO: Check for the path of the excel file, must be dynamically placed.

    :param defect_sheet_no: fed from sec3_driver_func()
    :return: returns pandas data frame of whole excel sheet.
    """
    df = pd.read_excel(excel_path, sheet_name=f"defect_map_page_{defect_sheet_no}")
    return df


def create_defect_table(doc_obj, defect_sheet_no, table_count, pier):
    """
    1. Calls the header row of the main defect table and adds further rows to it according to number of defects.
    2. Also collects some data related to defect type [maj, mod, min] for section 3.x.4 and returns it.
    :param doc_obj: doc object
    :param defect_sheet_no: fed from sec3_driver_func()
    :param table_count: table count from sec3_driver_func()
    :param pier: pier name
    :return: a list containing 3 dictionaries which has data related to total defects of certain type
             and name of the defects.

    TODO: 1. Reading some specific section of the excel sheet[5:12], if there is some change in the excel sheet
          then the function will break or might reads the wrong data.
          2. defect image restricted for .jpg
    """
    df = get_defect_excel_data(defect_sheet_no)
    # Defect Table Page starts
    try:
        total_defects = len(df["Defect ID"])
    except KeyError:
        total_defects = len(df["Observation ID"])
    # POPULATING TABLE
    total_tables = math.ceil(total_defects / 2)
    count = total_defects
    excel_row_count = 0
    maj_count = {"total": 0, "name": Counter([])}
    mod_count = {"total": 0, "name": Counter([])}
    min_count = {"total": 0, "name": Counter([])}
    for new_table in range(total_tables):
        table_defect = defect_table(doc_obj)  # Creating Header row of table
        new_row_dim = [1.24, 3.6, 8, 3.25, 4.75]
        try:
            df = df[["Defect ID", "Description", "Dive Location", "Depth(m)",	"Dimension(mm)", "Defect_ID", "Color_of_marker"]]
        except KeyError:
            df = df[["Observation ID", "Description", "Dive Location", "Depth(m)",	"Dimension(mm)", "Defect_ID", "Color_of_marker"]]
        if count == 1:
            row_range = 3
        else:
            row_range = 4
        for i in range(2, row_range):
            add_new_row(table_defect)
            row = table_defect.rows[i]
            row.height = Cm(6)
            for j in range(5):
                cell = table_defect.cell(i, j)
                cell.width = new_row_dim[j]
                cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
                para = cell.paragraphs[0]
                para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
                run_text = ""
                if j < 4:
                    if j == 0:  # Defect ID
                        run_text = df.iloc[excel_row_count][j]
                    if j == 1:  # Defect type / Severity
                        color = df.iloc[excel_row_count][-1].lower()
                        def_name = df.iloc[excel_row_count][j]
                        def_type = ""
                        if color == "yellow":
                            def_type = "Minor"
                            min_count["total"] += 1
                            min_count["name"][def_name] += 1
                        if color == "orange":
                            def_type = "Moderate"
                            mod_count["total"] += 1
                            mod_count["name"][def_name] += 1
                        if color == "red":
                            def_type = "Major"
                            maj_count["total"] += 1
                            maj_count["name"][def_name] += 1
                        run_text = def_name + "/" + def_type
                    if j == 2:  # Location & Depth
                        run_text = df.iloc[excel_row_count][j] + " & " + str(df.iloc[excel_row_count][j + 1])
                    if j == 3:  # Dimension of defects
                        if str(df.iloc[excel_row_count][j + 1]) == "nan":
                            run_text = "NA"
                        else:
                            run_text = df.iloc[excel_row_count][j + 1]
                    para.add_run(str(run_text))

                else:
                    para.add_run().add_picture(                     # Defect Image
                        r"{}".format(get_defects_img(df.iloc[excel_row_count][5] + ".jpg")), height=Cm(6.01),
                        width=Cm(11.31))

            excel_row_count += 1

        if count <= 2 or total_tables == 1:  # Check if iteration is at last row
            add_table_below_label(doc_obj, f"Table {table_count}: Observation details of {pier}")

        doc_obj.add_page_break()
        count -= 2

    return [maj_count, mod_count, min_count]


def scour_heading(doc_obj, sub_head_no, ss_head_no, pier):
    """
    Adds scour sub heading line.
    @param doc_obj: doc object
    @param sub_head_no: sub heading number
    @param ss_head_no: sub-sub heading number
    @param pier: pier name
    @return: heading paragraph
    """
    scour_head = add_section_ss_heading(doc_obj,
                                        f"3.{sub_head_no}.{ss_head_no}   Water depth sounding and scour details of {pier}")
    scour_head.paragraph_format.space_after = Pt(6)
    return scour_head


def scour_overview(doc_obj, fig_count, pier):
    doc_obj.add_paragraph()
    pier_num = pier.split(" ")[1]
    img_para = doc_obj.add_paragraph()  # Adding para for figure
    img_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    img_para.add_run().add_picture(get_scour_img(pier_num, ov=True), height=Cm(8.64), width=Cm(11.93))
    # add_fig_below_label(doc_obj, f"Figure {fig_count}: Scour Overview at {pier}")


def scour_table_1(doc_obj, table_count, pier):
    table = doc_obj.add_table(rows=1, cols=2, style="ESA_table_style_main")
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    merge_hor_cell(table, 0)  # merging first row cells

    df = check_scour_exist(pier, scour_table_1_data=True)
    cell = table.cell(0, 0)
    cell.width = Cm(16.98)
    row = table.rows[0]
    row.height = Cm(0.84)
    para = cell.paragraphs[0]
    para.add_run("Scour Depth Details")
    para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

    for i in range(4):
        row = table.add_row()
        row.height = Cm(0.84)
        for j in range(2):
            cell = table.cell(i + 1, j)
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            if j == 0:
                cell.width = Cm(4.7)
            if i == 0 and j == 1:
                para.add_run(str(df.iloc[i, j]) + " m from rail level")
            elif j != 0:
                para.add_run(str(df.iloc[i, j]) + " m")
            else:
                para.add_run(str(df.iloc[i, j]))

    add_table_below_label(doc_obj, f"Table {table_count}: Scour depth details")


def scour_detailed_view(doc_obj, fig_count, pier):
    doc_obj.add_paragraph()
    pier_num = pier.split(" ")[1]
    img_para = doc_obj.add_paragraph()  # Adding para for figure
    img_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    img_para.add_run().add_picture(get_scour_img(pier_num, dv=True), height=Cm(10.26), width=Cm(14.79))
    # add_fig_below_label(doc_obj, f"Figure {fig_count}: Scour Overview at {pier}")


def scour_table_2(doc_obj, table_count, pier):
    table = doc_obj.add_table(rows=1, cols=2, style="ESA_table_style_main")
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    merge_hor_cell(table, 0)  # merging first row cells

    df = check_scour_exist(pier, scour_table_2_data=True)
    cell = table.cell(0, 0)
    cell.width = Cm(16.98)
    row = table.rows[0]
    row.height = Cm(0.84)
    para = cell.paragraphs[0]
    para.add_run("Scour Depth Details")
    para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

    for i in range(2):
        row = table.add_row()
        row.height = Cm(0.84)
        for j in range(2):
            cell = table.cell(i + 1, j)
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            if j == 0:
                cell.width = Cm(5.71)
            if j != 0:
                para.add_run(str(df.iloc[i, j]) + " m")
            else:
                para.add_run(str(df.iloc[i, j]))

    add_table_below_label(doc_obj, f"Table {table_count}: Scour depth details")


def scour_table_3(doc_obj, table_count, pier):
    table = doc_obj.add_table(rows=1, cols=2, style="ESA_table_style_main")
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    left_cell = table.cell(0, 0)
    left_cell.width = Cm(5.71)
    right_cell = table.cell(0, 1)
    right_cell.width = Cm(12.78)
    row = table.rows[0]
    row.height = Cm(1.26)
    # change color fill of a cell https://stackoverflow.com/a/43467445
    shading_elm_1 = parse_xml(r'<w:shd {} w:fill="DBEFE9"/>'.format(nsdecls('w')))
    right_cell._tc.get_or_add_tcPr().append(shading_elm_1)

    df = check_scour_exist(pier, scour_table_3_data=True)
    left_para = left_cell.paragraphs[0]
    left_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    left_para.add_run(str(df.iloc[0, 0]))
    right_para = right_cell.paragraphs[0]
    right_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    run = right_para.add_run(str(df.iloc[0, 1]) + " m")
    font = run.font
    font.bold = False
    font.color.rgb = RGBColor(0, 0, 0)

    add_table_below_label(doc_obj, f"Table {table_count}: Mid Span Sounding details")


def scour_struct_assessment(doc_obj, scour_head_no, pier):
    df = check_scour_exist(pier, scour_table_1_data=True)
    x = df.iloc[1, 1]
    y = df.iloc[2, 1]
    z = df.iloc[3, 1]

    para = doc_obj.add_paragraph(style="List Bullet 3")
    para.paragraph_format.line_spacing = Pt(18.5)
    run = para.add_run(f"From the water depth sounding data, the average depth of bed around the pier is observed at "
                       f"{x} m with respect to the rail level. The maximum scour depth around the pier is found to be {y} m "
                       f"as shown in section {scour_head_no.text.split(' ')[0]}. Thus, the depth of scour hole is found to be {z} m.")
    return run


def struct_assessment_head(doc_obj, sub_head_no, ss_head_no):
    """Structural Assessment heading"""
    struct_head = add_section_ss_heading(doc_obj, f"3.{sub_head_no}.{ss_head_no} Structural condition assessment")
    struct_head.paragraph_format.space_after = Pt(6)
    return struct_head


def struct_assessment(doc_obj, maj_count, mod_count, min_count):
    """
    1. Creates all the bullet list according to defects in the pier.
    2. Defect type, defect name and at how many location defects are there are calculated.
    3. Changes color of the font according to the defect type.
    4. Was/were, singular/plural logic is also given.

    TODO: 1. Could be optimized. Have to divide it into 5 runs. Is there any better approach ?
          2. When Cavity plus Cavity and honeycomb formation both are present then print only cavity and honeycomb part.
    """
    defect_type_name = {}
    if maj_count["total"] > 0:
        defect_type_name["Major"] = "red"

    if mod_count["total"] > 0:
        defect_type_name["Moderate"] = "orange"
    if min_count["total"] > 0:
        defect_type_name["Minor"] = "yellow"

    for idx in range(len(defect_type_name)):
        para = doc_obj.add_paragraph(style="List Bullet 3")
        para.add_run("In the area inspected ")  # run1
        add_defect_colored(para, list(defect_type_name)[idx], bold=True)  # run2
        if "Major" == list(defect_type_name.keys())[idx]:
            defect_name = [name for name in maj_count["name"].keys()]

            # run3 has 3 possibilities
            para.add_run(
                f" observations pertaining to "
                f"{' & '.join(defect_name).title().strip() if len(maj_count['name'].keys()) > 1 else ' '.join(defect_name).title().strip()} "
                f"{'were' if maj_count['total'] > 1 else 'was'} observed at {maj_count['total']} "
                f"{'locations' if maj_count['total'] > 1 else 'location'}. (Refer observations marked in ")
        elif "Moderate" == list(defect_type_name.keys())[idx]:
            defect_name = [name for name in mod_count["name"].keys()]
            para.add_run(
                f" observations pertaining to "
                f"{' & '.join(defect_name).title().strip() if len(mod_count['name'].keys()) > 1 else ' '.join(defect_name).title().strip()} "
                f"{'were' if mod_count['total'] > 1 else 'was'} observed at {mod_count['total']} "
                f"{'locations' if mod_count['total'] > 1 else 'location'}. (Refer observations marked in ")
        elif "Minor" == list(defect_type_name.keys())[idx]:
            defect_name = [name for name in min_count["name"].keys()]
            para.add_run(
                f" observations pertaining to "
                f"{' & '.join(defect_name).title().strip() if len(min_count['name'].keys()) > 1 else ' '.join(defect_name).title().strip()} "
                f"{'were' if min_count['total'] > 1 else 'was'} observed at {min_count['total']} "
                f"{'locations' if min_count['total'] > 1 else 'location'}. (Refer observations marked in ")

        add_defect_colored(para, list(defect_type_name)[idx], color=True,
                           color_name=list(defect_type_name.values())[idx])  # run4
        para.add_run(" markers)")  # run5
        para.paragraph_format.line_spacing = Pt(18.5)


def recommend_heading(doc_obj, sub_head_no, ss_head_no, struct_head_no, pier):
    """
    Creates the recommendation section.
    :param doc_obj:
    :param sub_head_no:
    :param ss_head_no:
    :param struct_head_no: takes input from sec3_Driver_func()
    :param pier:
    :return:
    """
    para = add_section_ss_heading(doc_obj, f"3.{sub_head_no}.{ss_head_no} Recommendations")
    para.paragraph_format.space_after = Pt(10)
    para = doc_obj.add_paragraph(style="List Bullet 3")
    para.add_run(f"Necessary steps should be taken to rectify the observations described in section"
                 f" {struct_head_no.text.split(' ')[0]} made on {pier} to prevent "
                 f"further deterioration as per the remedial measures guide. ")


def sec3_driver_func(doc_obj):
    """
    The most important function of the section 3.0 which puts together all the above defined functions and iterates
    according to number of piers.

    TODO: 1. Pier information is taken from the pier image directory can be calculated from excel sheet.
          2. Check for any optimization need.
    :param doc_obj:
    :return: a dictionary containing figure and table count to pass on to the next section.
    """
    sub_head_no = 1
    ss_head_no = 1
    fig_count = 1
    table_count = 4
    defect_sheet_no = 1
    paths = get_pier_path(get_path=True)  # Getting all images path of current pier
    pier_ls = get_pier_path(pier_names=True)

    for pier in pier_ls:
        # Pier dive detail
        pier_headings(doc_obj, sub_head_no, ss_head_no, pier)
        ss_head_no += 1
        add_dive_fig(doc_obj, fig_count, paths, pier_ls, pier)
        fig_count += 1

        # Element description table
        element_table(doc_obj, sub_head_no, ss_head_no, table_count, pier)
        ss_head_no += 1
        table_count += 1

        # IF THERE IS NO DEFECT IN THE PIER
        if list(paths.values())[pier_ls.index(pier)] is None:
            # CASE:A --> NO DEFECT BUT SCOUR EXIST
            if check_scour_exist(pier, check=True):
                change_orientation(doc_obj, landscape=1)
                scour_head_no = scour_heading(doc_obj, sub_head_no, ss_head_no, pier)
                ss_head_no += 1
                # Add scour img #1
                scour_overview(doc_obj, fig_count, pier)
                # fig_count += 1    # No need of counting this image
                # Add scour table #1
                scour_table_1(doc_obj, table_count, pier)
                table_count += 1
                # Add scour img #2
                scour_detailed_view(doc_obj, fig_count, pier)
                # fig_count += 1    # No need of counting this image
                # Add scour table #2
                scour_table_2(doc_obj, table_count, pier)
                table_count += 1
                # Add scour table #3
                scour_table_3(doc_obj, table_count, pier)
                table_count += 1
                change_orientation(doc_obj, portrait=1)
                # Struct Assessment
                struct_head_no = struct_assessment_head(doc_obj, sub_head_no, ss_head_no)
                scour_struct_assessment(doc_obj, scour_head_no, pier)
                ss_head_no += 1
                # Empty paragraph
                para = doc_obj.add_paragraph()
                para.paragraph_format.line_spacing = Pt(1)
                # Recommend
                recommend_heading(doc_obj, sub_head_no, ss_head_no, struct_head_no, pier)

                doc_obj.add_page_break()
                sub_head_no += 1
                ss_head_no = 1
            # CASE:B --> NO DEFECT NO SCOUR
            else:
                doc_obj.add_page_break()
                sub_head_no += 1
                ss_head_no = 1
            continue

        # Observation Page starts
        observation_page(doc_obj, sub_head_no, ss_head_no, paths, pier_ls, pier, fig_count)
        ss_head_no += 1
        fig_count += 1
        doc_obj.add_page_break()

        # Defect Table Page Starts
        arr = create_defect_table(doc_obj, defect_sheet_no, table_count, pier)
        table_count += 1
        defect_sheet_no += 1  # Going to next excel sheet
        # CASE:C --> BOTH DEFECT & SCOUR EXIST
        if check_scour_exist(pier, check=True):
            scour_head_no = scour_heading(doc_obj, sub_head_no, ss_head_no, pier)
            ss_head_no += 1
            # Add scour img #1
            scour_overview(doc_obj, fig_count, pier)
            # Add scour table #1
            scour_table_1(doc_obj, table_count, pier)
            table_count += 1
            # Add scour img #2
            scour_detailed_view(doc_obj, fig_count, pier)
            # Add scour table #2
            scour_table_2(doc_obj, table_count, pier)
            table_count += 1
            # Add scour table #3
            scour_table_3(doc_obj, table_count, pier)
            table_count += 1
        # CASE:D --> DEFECT EXIST BUT NO SCOUR
        else:
            pass

        change_orientation(doc_obj, portrait=1)

        # Structural Condition Assessment Page
        struct_head_no = struct_assessment_head(doc_obj, sub_head_no, ss_head_no)  # will add heading and also return the text of heading
        ss_head_no += 1
        struct_assessment(doc_obj, maj_count=arr[0], mod_count=arr[1], min_count=arr[2])
        # Scour Structural assessment
        if check_scour_exist(pier, check=True):
            scour_struct_assessment(doc_obj, scour_head_no, pier)
        else:
            pass
        # Empty paragraph
        para = doc_obj.add_paragraph()
        para.paragraph_format.line_spacing = Pt(1)
        # Recommendation Heading
        recommend_heading(doc_obj, sub_head_no, ss_head_no, struct_head_no, pier)
        ss_head_no = 1

        doc_obj.add_page_break()
        sub_head_no += 1

    return {"fig_count": fig_count, "table_count": table_count}
