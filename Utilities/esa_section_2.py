#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_section_2.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This is the main script that compile all the sections together and generates the final report docx file."""
# '''===================================================================================================================
import sys

from Utilities.esa_utils import add_section_heading, heading_font_change, merge_hor_cell, set_cell_border
from Utilities.esa_utils import add_section_sub_heading, add_table_below_label, total_piers_inspected, check_excel_file_path
from Utilities.esa_image_utils import icons_path
from Utilities.opensource_scripts.floating_picture import add_float_picture
from docx.shared import RGBColor, Pt, Cm
from docx.enum.table import WD_TABLE_ALIGNMENT, WD_CELL_VERTICAL_ALIGNMENT
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
import pandas as pd
import numpy as np


excel_path = sys.argv[1]+"\\files\\Resources\\Automation_template.xlsx"
check_excel_file_path()


def section_2_heading(doc_obj):
    """
    Creates section top heading and adds red line below.
    """
    para = add_section_heading(doc_obj, "2.0 Introduction")
    heading_font_change(para, "Georgia", Pt(20), True, 0, 0, 0)


def get_data_for_table1():
    df1 = pd.read_excel(excel_path, sheet_name="introduction", usecols="A:B")
    df2 = pd.read_excel(excel_path, sheet_name="introduction", usecols="C:D")
    df3 = pd.read_excel(excel_path, sheet_name="introduction", usecols="E:F")

    arr = [df1, df2, df3]
    data = []
    for df in arr:
        df = df.replace({np.nan: None})
        if df.columns[0] == "Structure Details":
            pass
        else:
            data.append((df.columns[0], ""))

        for k, v in df.values:
            if k is None:
                continue
            elif v is None:
                data.append((k, "NIL"))
            else:
                data.append((k, v))

    return data


def sec_2_1_table(doc_obj):
    """
    1. Creates the background/obj table.
    2. Merge required cells
    3. Adds data to the cell.
    TODO: 1. Table has static data, needs to be dynamic once we have the data.
    """
    add_section_sub_heading(doc_obj, "2.1 Background / Objective")
    # Adding table
    objective_table = doc_obj.add_table(rows=11, cols=2)
    objective_table.style = "ESA_table_style_main"
    objective_table.width = Cm(17.51)
    objective_table.alignment = WD_TABLE_ALIGNMENT.LEFT
    # Merging cells
    merge_hor_cell(objective_table, 0)
    merge_hor_cell(objective_table, 6)
    merge_hor_cell(objective_table, 8)

    total_piers = total_piers_inspected()
    data = get_data_for_table1()[:8]
    data.append(("Area of inspection", ""))
    data.append(("Number of the members cleaned", f"{total_piers} Piers"))
    data.append(("Number of the members inspected", f"{total_piers} Piers"))

    for i in range(11):
        row = objective_table.rows[i]
        row.height = Cm(1)
        for j in range(2):
            cell = objective_table.cell(i, j)
            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            para = cell.paragraphs[0]
            para.add_run(str(data[i][j]))
            if data[i][j] == "":
                para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

    add_table_below_label(doc_obj, "Table 1: Background / Objective")
    doc_obj.add_paragraph()


def sec_2_2_table(doc_obj):
    """
    1. Creates the 2nd table of the section.
    2. Changes color fill of a cell because default is too dark.
    TODO: 1. Data is static again, needs to be dynamic.
    """
    add_section_sub_heading(doc_obj, "2.2 Scope of Work")
    # Scope Table
    scope_table = doc_obj.add_table(rows=1, cols=2)
    scope_table.style = "ESA_table_style_main"
    scope_table.width = Cm(17.56)
    row = scope_table.rows[0]
    row.height = Cm(1.53)
    scope_table.alignment = WD_TABLE_ALIGNMENT.CENTER

    # change color fill of a cell https://stackoverflow.com/a/43467445
    shading_elm_1 = parse_xml(r'<w:shd {} w:fill="DBEFE9"/>'.format(nsdecls('w')))
    scope_table.cell(0, 1)._tc.get_or_add_tcPr().append(shading_elm_1)

    df = pd.read_excel(excel_path, sheet_name="executive_summary")
    # Populating table
    data = [("Purpose", df.iloc[0]["Area of Interest"])]
    for i in range(1):
        for j in range(2):
            cell = scope_table.cell(i, j)
            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            run = para.add_run(str(data[i][j]))
            font = run.font
            if j == 0:
                cell.width = Cm(5)
            if j == 1:
                cell.width = Cm(10.92)
                para.alignment = WD_PARAGRAPH_ALIGNMENT.LEFT
                font.bold = False
                font.color.rgb = RGBColor(0, 0, 0)

    add_table_below_label(doc_obj, "Table 2: Scope of work undertaken")

    doc_obj.add_page_break()


def sec_2_3_table(doc_obj):
    """
    1. Creates the structure table.
    2. Set the dimensions.
    3. Insert the icons.
    4. Color the border and remove the border where required
    5. Populate the table
    TODO: 1. Static data --> Dynamic
          2. Need of breaking the functionalities into pieces ?
          3. icon paths needs to to dynamic.
    """
    add_section_sub_heading(doc_obj, "2.3 Structure Details")
    # structure_table
    struct_table = doc_obj.add_table(rows=9, cols=2)
    struct_table.style = "Table Grid"
    struct_table.autofit = False
    struct_table.allow_autofit = False

    # Setting Table Dimensions
    for row in struct_table.rows:
        row.height = Cm(1.4)

    # Inserting Icons to Table
    paths = icons_path()[-1]
    i = 0
    for path in paths:
        cell = struct_table.cell(i, 0)
        para = cell.paragraphs[0]
        add_float_picture(para, r"{}".format(path.rstrip()), width=Cm(1.1), height=Cm(1.1), pos_x=Cm(0.2),
                          pos_y=Cm(-0.1))
        i += 1
        if i == len(paths):
            break
    # Populating table
    data = get_data_for_table1()[8:]
    for i in range(len(data)):
        for j in range(2):
            cell = struct_table.cell(i, j)
            set_cell_border(cell, top={"sz": 10, "val": "single", "color": "#FF0000"},
                            bottom={"sz": 10, "color": "#FF0000", "val": "single"},
                            start={"sz": 10, "color": "#FF0000", "val": "single"},
                            end={"sz": 10, "color": "#FF0000", "val": "single"})
            if j == 0:
                set_cell_border(cell, start={"sz": 10, "color": "#FF0000", "val": "nil"})
            else:
                set_cell_border(cell, end={"sz": 10, "color": "#FF0000", "val": "nil"})
            if i == 0 and j <= 1:
                set_cell_border(cell, top={"sz": 10, "val": "nil", "color": "#FF0000"})
            # elif i == 8 and j <= 1:
            #     set_cell_border(cell, bottom={"sz": 10, "val": "nil", "color": "#FF0000"})

            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            cell.width = Cm(8.8)
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            para.add_run(str(data[i][j]))

    doc_obj.add_paragraph()
    add_table_below_label(doc_obj, "Table 3: Details of bridge No 44")

    doc_obj.add_page_break()
