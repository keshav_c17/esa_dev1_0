#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_import_modules.py
# @Version : 1.0 
# @Date    : 22-12-2021
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" MODULE INFORMATION """
# '''===================================================================================================================

from docx import Document
from docx.enum.section import WD_SECTION_START
from update_toc_script import update_toc
from Utilities.esa_all_toc_implement import add_main_toc, add_figure_toc, add_table_toc, add_videos_toc
from Utilities.esa_utils import define_text_styles, create_page_layout, add_header, add_footer, fill_templates
from Utilities.esa_section_1 import section_1_heading, creating_top_table, adding_graph_images, sec_1_last_table, create_align_table
from Utilities.esa_section_2 import section_2_heading, sec_2_1_table, sec_2_2_table, sec_2_3_table
from Utilities.esa_section_3 import section_3_heading, sec3_driver_func
from Utilities.esa_section_4 import section_4_heading, heading_4_1, heading_4_2, copy_spec_table, heading_4_2_1
from Utilities.esa_section_4 import heading_4_2_2, heading_4_2_3, table_4_3, table_4_4, weather_table
from Utilities.esa_section_5 import section_5_heading, section_5_sub_heading, sec5_driver_func
from Utilities.esa_section_6 import section_6_heading, sec6_driver_func
import argparse
