#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_all_toc_implement.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This module implement all of the table of contents from the source file of toc."""
# '''===================================================================================================================
from Utilities.esa_utils import heading_font_change, insertHR
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.shared import Pt
# from docx.oxml.ns import qn
# from docx.oxml import OxmlElement
from Utilities.opensource_scripts.create_toc_source import create_toc


# def add_bookmark(paragraph, bookmark_text, bookmark_name):
#     """
#     Original Author refer: https://stackoverflow.com/q/57586400
#     :param paragraph:
#     :param bookmark_text:
#     :param bookmark_name:
#     :return:
#     """
#     run = paragraph.add_run()
#     tag = run._r  # for reference the following also works: tag =  document.element.xpath('//w:r')[-1]
#     start = OxmlElement('w:bookmarkStart')
#     start.set(qn('w:id'), '0')
#     start.set(qn('w:name'), bookmark_name)
#     tag.append(start)
#
#     text = OxmlElement('w:r')
#     text.text = bookmark_text
#     tag.append(text)
#
#     end = OxmlElement('w:bookmarkEnd')
#     end.set(qn('w:id'), '0')
#     end.set(qn('w:name'), bookmark_name)
#     tag.append(end)


def add_main_toc(doc_obj):
    para = doc_obj.add_heading("TABLE OF CONTENTS", 0)
    para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    heading_font_change(para, "Georgia", Pt(24), bold=True)
    insertHR(para)
    create_toc(doc_obj)
    doc_obj.add_page_break()


def add_figure_toc(doc_obj):
    # List of Figures/Photographs
    para = doc_obj.add_heading("List of Figures/Photographs".upper(), 0)
    para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    heading_font_change(para, "Georgia", Pt(20), bold=True)
    insertHR(para)
    create_toc(doc_obj, figure_toc=True)
    doc_obj.add_page_break()


def add_table_toc(doc_obj):
    para = doc_obj.add_heading("List of Tables".upper(), 0)
    para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    heading_font_change(para, "Georgia", Pt(20), bold=True)
    insertHR(para)
    create_toc(doc_obj, table_toc=True)
    doc_obj.add_page_break()


def add_videos_toc(doc_obj):
    para = doc_obj.add_heading("List of Videos".upper(), 0)
    para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    heading_font_change(para, "Georgia", Pt(20), bold=True)
    insertHR(para)
    create_toc(doc_obj, video_toc=True)
    doc_obj.add_page_break()

