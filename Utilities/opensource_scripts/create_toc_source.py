"""
original: https://github.com/python-openxml/python-docx/issues/36

list_of_figures_toc: https://github.com/python-openxml/python-docx/issues/723#issuecomment-674466747
Created on Sat Aug 15 21:40:26 2020
@author: Xiaomin Zhao

Edited on Sat Oct 23 12:57:30 2021
@custom_edits_by: Keshav Choudhary, SE @Planys_Technologies, Chennai
changes: added paragraph style on which ToC needs to be created (e.g. "Body Text", "Body Text 2")

"""
from docx.oxml.ns import qn
from docx.oxml import OxmlElement


def create_toc(document, figure_toc=False, table_toc=False, video_toc=False):
    paragraph = document.add_paragraph()
    run = paragraph.add_run()
    fldChar = OxmlElement('w:fldChar')  # creates a new element
    fldChar.set(qn('w:fldCharType'), 'begin')  # sets attribute on element
    fldChar.set(qn('w:dirty'), 'true')  # https://github.com/python-openxml/python-docx/issues/36#issuecomment-656127078
    instrText = OxmlElement('w:instrText')
    instrText.set(qn('xml:space'), 'preserve')  # sets attribute on element
    if figure_toc:
        instrText.text = 'TOC \\t "Body Text" \\h \\z'
    elif video_toc:
        instrText.text = 'TOC \\t "Intense Quote,1,Quote,2" \\h \\z'
    elif table_toc:
        instrText.text = 'TOC \\t "Body Text 2" \\h \\z'
    else:
        instrText.text = 'TOC \\t "Heading 1,1,Heading 2,2, Heading 3,3, Intense Quote,1,Quote,2" \\h \\z '   # change 1-3 depending on heading levels you need

    fldChar2 = OxmlElement('w:fldChar')
    fldChar2.set(qn('w:fldCharType'), 'separate')
    fldChar3 = OxmlElement('w:t')
    fldChar3.text = "Right-click to update field."
    fldChar2.append(fldChar3)

    fldChar4 = OxmlElement('w:fldChar')
    fldChar4.set(qn('w:fldCharType'), 'end')

    fldChar3.fldChar3 = OxmlElement('w:updateFields')
    fldChar3.set(qn('w:val'), 'true')

    r_element = run._r
    r_element.append(fldChar)
    r_element.append(instrText)
    r_element.append(fldChar2)
    r_element.append(fldChar4)
