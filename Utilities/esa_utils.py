#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_utils.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This is a very important module of the project that contains many tools and functionalities that is used in all of
    the sections of the report."""
# '''===================================================================================================================
import os
import sys
from docxtpl import DocxTemplate
from docx.oxml.shared import OxmlElement
from docx.oxml.ns import qn
from docx.shared import RGBColor, Pt, Cm, Mm
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
from docx.enum.section import WD_SECTION_START, WD_ORIENTATION
from Utilities.opensource_scripts.floating_picture import add_float_picture
from docx.enum.table import WD_CELL_VERTICAL_ALIGNMENT
from docx.oxml import parse_xml
from docx.oxml.ns import nsdecls
import pandas as pd
from openpyxl import load_workbook

excel_path = sys.argv[1]+"\\files\\Resources\\Automation_template.xlsx"


df = pd.read_excel(excel_path, sheet_name="landing_page")
df2 = pd.read_excel(excel_path, sheet_name="executive_summary").dropna()
footer_text = df2["footer_text"][0].split("-")
first = "-".join(footer_text[:2]) + " -"
second = " " + "".join(footer_text[2])
footer_data = [(first, second), ("Report No:", f" {df.iloc[0]['Report_file_name']}")]


def check_excel_file_path():
    if os.path.isfile(excel_path):
        pass
    else:
        raise FileNotFoundError("Path to the automation_excel file is incorrect.")

check_excel_file_path()


def total_piers_inspected():
    wb = load_workbook(excel_path, read_only=True, keep_links=False)
    last_insp_page = wb.sheetnames[wb.sheetnames.index("defect_map_page_1") - 1]
    total_piers = last_insp_page.split('_')[-1]
    return total_piers


# def resolve_path(path):
#     if getattr(sys, "frozen", False):
#         # If the 'frozen' flag is set, we are in bundled-app mode!
#         resolved_path = os.path.join(sys._MEIPASS, path)
#     else:
#         # Normal development mode. Use os.getcwd() or __file__ as appropriate in your case...
#         resolved_path = os.path.join(os.getcwd(), path)
#
#     return resolved_path
def get_correct_path(relative_path):
    try:
        base_path = sys._MEIPASS

    except Exception:
        base_path = os.path.abspath(".")

    return base_path+relative_path


def fill_templates():
    """
    This function opens up the template docx file-->get required data from excel-->fill into the template-->save
    """
    # doc = DocxTemplate(os.getcwd()+"\\Resources\\ESA_template.docx")
    doc = DocxTemplate(get_correct_path("\\Resources\\ESA_template.docx"))

    context = {'title': df2["front_page_title"].dropna()[0], 'client': df2["client_name"].dropna()[0], 'location': df["Location"].dropna()[0]}
    doc.render(context)
    doc.save(f"ESA_[{sys.argv[2]}].docx")


def define_text_styles(doc_obj):
    """
    This function defines all the required text styles used in the document.
    [All the styles mentioned in the list below will have same font name and size.]
    Body Text              --> used in figures label text
    Body Text 2            --> used in table label text
    List Bullet 3          --> used in bullet list in section 3.x.4
    Normal                 --> when normally type any text in the document.
    Medium Grid 3 Accent 1 --> used in all the tables
    Table Grid             --> used in first two sections [1 & 2]
    ## [TODO] If certain style needs other font name or size then we have to create it separately.
    """
    style_names = ["Body Text", "Body Text 2", 'List Bullet 3', "Normal", "Medium Grid 3 Accent 1", "Table Grid"]
    for name in style_names:
        style = doc_obj.styles[name]
        font = style.font
        font.name = "Georgia"
        font.size = Pt(12)

########################################################################################################################
#                                        PAGE NUMBER                                                                   #
########################################################################################################################


def create_element(name):
    return OxmlElement(name)


def create_attribute(element, name, value):
    element.set(qn(name), value)


def add_page_number(run):
    """
    This function adds page number to the docx file in the footer section.

    refer Original Author: https://stackoverflow.com/a/56676220

    :param run: Insert footer paragraph run.
    :return: NIL
    """
    fldChar1 = create_element('w:fldChar')
    create_attribute(fldChar1, 'w:fldCharType', 'begin')

    instrText = create_element('w:instrText')
    create_attribute(instrText, 'xml:space', 'preserve')
    instrText.text = "PAGE"

    fldChar2 = create_element('w:fldChar')
    create_attribute(fldChar2, 'w:fldCharType', 'end')

    run._r.append(fldChar1)
    run._r.append(instrText)
    run._r.append(fldChar2)

########################################################################################################################


def insertHR(paragraph):
    """
    Adds a red line under the title, generally used for title

    refer Original author: https://github.com/python-openxml/python-docx/issues/105#issuecomment-442786431

    :param paragraph: Enter title_paragraph under which line is needed.
    :return: Only do changes to the given paragraph.
    """
    p = paragraph._p  # p is the <w:p> XML element
    pPr = p.get_or_add_pPr()
    pBdr = OxmlElement('w:pBdr')
    pPr.insert_element_before(pBdr,
                              'w:shd', 'w:tabs', 'w:suppressAutoHyphens', 'w:kinsoku', 'w:wordWrap',
                              'w:overflowPunct', 'w:topLinePunct', 'w:autoSpaceDE', 'w:autoSpaceDN',
                              'w:bidi', 'w:adjustRightInd', 'w:snapToGrid', 'w:spacing', 'w:ind',
                              'w:contextualSpacing', 'w:mirrorIndents', 'w:suppressOverlap', 'w:jc',
                              'w:textDirection', 'w:textAlignment', 'w:textboxTightWrap',
                              'w:outlineLvl', 'w:divId', 'w:cnfStyle', 'w:rPr', 'w:sectPr', 'w:pPrChange')
    bottom = OxmlElement('w:bottom')
    bottom.set(qn('w:val'), 'single')
    bottom.set(qn('w:sz'), '15')
    bottom.set(qn('w:space'), '7')
    bottom.set(qn('w:color'), 'FF0000')
    pBdr.append(bottom)


def heading_font_change(heading_para, name, size,  bold=False, r=0, g=0, b=0):
    """
    Only used for heading = document.add_heading("TEXT", level=0)
    This function will customize the level 0 heading color and other
    font related things which is not possible by default.
    [LEVEL 0 style is "Title" and by default color is light blue with a blue line below it.]
    :param heading_para: Enter the heading paragraph.
    :param name: font name
    :param size: font size
    :param bold: bold or normal
    :param r: red color value
    :param g: green color value
    :param b: blue color value
    :return: return nothing only do changes to given heading paragraph
    """
    title_style = heading_para.style
    title_style.font.name = name
    title_style.font.size = size
    title_style.font.bold = bold
    heading_para.style.font.color.rgb = RGBColor(r, g, b)
    rFonts = title_style.element.rPr.rFonts
    rFonts.set(qn("w:asciiTheme"), name)


def merge_hor_cell(table, row_num: int):
    """
    Merge two or four horizontal cells of a table.
    :param table: Enter table object in which merging is needed
    :param row_num: Enter row number where cells needed to be merged
    :return: returns nothing only do changes to the table
    """
    row_to_merge = table.rows[row_num]
    total_cells = len(row_to_merge.cells)
    if total_cells == 2:
        c0, c1 = row_to_merge.cells[:]
        c0.merge(c1)
    if total_cells == 4:
        c0, c1 = row_to_merge.cells[:2]
        c0.merge(c1)
        c2, c3 = row_to_merge.cells[2:4]
        c2.merge(c3)


def merge_ver_cell(table, col_num: int):
    """
    Merge two vertical cells [column wise]
    :param table: Table name.
    :param col_num: Column number in which cells are located.
    :return:
    """
    col_to_merge = table.columns[col_num]
    c0, c1 = col_to_merge.cells[:2]
    c0.merge(c1)


def cell_run_font(run):
    """
    Changes the cell text's font and size.
    :param run: Enter cell text run.
    :return:
    """
    font = run.font
    font.size = Pt(12)
    font.name = "Georgia"


def add_section_heading(doc_obj, heading_text: str):
    """
    # [TODO] Further inspection needed like where it is used in the program
            and is it needed or not.
    Adds top heading with level 1 and inserts a red line below the heading.
    :param doc_obj: doc object
    :param heading_text: Texts of the heading.
    :return: return a paragraph if you need to do further customization to it.
    """
    heading_para = doc_obj.add_heading(heading_text, 1)
    heading_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    insertHR(heading_para)  # Inserting red line below the heading
    return heading_para


def add_section_sub_heading(doc_obj, sub_heading_text: str):
    """
    Adds a sub heading with level 2 and some space after the heading.
    :param doc_obj: doc object
    :param sub_heading_text: TEXT
    :return: return the paragraph
    """
    sub_head_para = doc_obj.add_heading(sub_heading_text, 2)
    sub_head_para.style = doc_obj.styles["Heading 2"]
    heading_font_change(sub_head_para, "Georgia", size=Pt(16), bold=False)
    sub_head_para.paragraph_format.space_after = Pt(12)
    return sub_head_para


def add_section_ss_heading(doc_obj, sub_heading_text: str, font_size=Pt(14)):
    """
    Adds sub-sub heading with level 3.
    :param doc_obj: doc object.
    :param sub_heading_text: TEXT
    :param font_size: Font size.
    :return: returns a paragraph object.
    """
    ss_head_para = doc_obj.add_heading(sub_heading_text, 3)
    ss_head_para.style = doc_obj.styles["Heading 3"]
    heading_font_change(ss_head_para, "Georgia", font_size, False)
    ss_head_para.paragraph_format.line_spacing = 1
    return ss_head_para


def create_page_layout(doc_obj):
    """
    It sets the page layout like orientation, margins, header-footer distance, page size.
    Everything is pre-define/hardcoded according to the Ennore report docx file.
    :param doc_obj:
    :return:
    """
    main_section = doc_obj.sections[-1]
    main_section.orientation = WD_ORIENTATION.PORTRAIT
    main_section.top_margin = Cm(2.25)
    main_section.bottom_margin = Cm(1.59)
    main_section.left_margin = Cm(2.54)
    main_section.right_margin = Cm(2.54)
    main_section.page_height = Mm(297)
    main_section.page_width = Mm(210)
    main_section.footer_distance = Cm(0)
    main_section.header_distance = Cm(1.27)


def indent_table(table, indent):
    """
    Indents the table position.

    refer Original Author: https://stackoverflow.com/a/55142896

    :param table: table to indent
    :param indent: amount of indent in Pt, Cm, Mm
    :return:
    """
    # noinspection PyProtectedMember
    tbl_pr = table._element.xpath('w:tblPr')
    if tbl_pr:
        e = OxmlElement('w:tblInd')
        e.set(qn('w:w'), str(indent))
        e.set(qn('w:type'), 'dxa')
        tbl_pr[0].append(e)


def add_header(section_nam, landscape=False):
    """
    ## [TODO] Paths needs to be dynamic right now hard-coded
    Creates a header with planys logo.
    :param section_nam: section needs to be mention where header will be added.
    :param landscape: change in header logo dimension for landscape page.
    :return:
    """
    logo_path = get_correct_path("\\Resources\\header_footer_img\\header_logo.png")
    header = section_nam.header
    header.is_linked_to_previous = False
    header_para = header.paragraphs[0]
    # Function call to add float picture
    # https://github.com/dothinking/pdf2docx/issues/54#issuecomment-715925252
    if landscape:
        add_float_picture(header_para, logo_path, width=Cm(2.25), pos_x=Cm(26.57), pos_y=Cm(0.3))
    else:
        add_float_picture(header_para, logo_path, width=Cm(2.25), pos_x=Cm(18.38), pos_y=Cm(0.46))


def add_footer(section_name):
    """
    ## [TODO] 1. Paths needs to be dynamic right now hard-coded
              2. Footer text needs to be dynamic once received the data

    Creates the footer with planys logo and another image above it.
    :param section_name:
    :return:
    """
    footer = section_name.footer
    footer.is_linked_to_previous = False
    footer_para = footer.paragraphs[0]
    # PAGE NUMBER
    add_page_number(footer_para.add_run())  # https://stackoverflow.com/a/56676220
    footer_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

    logo_path1 = get_correct_path("\\Resources\\header_footer_img\\footer_behind_trans.png")
    logo_path2 = get_correct_path("\\Resources\\header_footer_img\\footer_front_trans.png")
    add_float_picture(footer_para, logo_path1, width=Cm(7.48), height=Cm(6.68), pos_x=Cm(-3.97), pos_y=Cm(26.38))
    add_float_picture(footer_para, logo_path2, width=Cm(4.06), height=Cm(4.93), pos_x=Cm(-0.26), pos_y=Cm(26.22))
    foot_table = footer.add_table(rows=1, cols=2, width=Cm(10))
    foot_table.autofit = False
    foot_table.allow_autofit = False
    indent_table(foot_table, Pt(5))  # https://stackoverflow.com/a/55142896

    data = footer_data
    for row in range(1):
        for col in range(2):
            cell = foot_table.cell(row, col)
            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            para = cell.paragraphs[0]
            if row == 0 and col == 1:
                cell.width = Cm(15.56)
                para.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
            else:
                cell.width = Cm(6)
                para.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
            run1 = para.add_run(str(data[col][row]))
            font = run1.font
            font.size = Pt(12)
            font.name = "Georgia"
            font.bold = True
            font.color.rgb = RGBColor(255, 0, 0)
            run2 = para.add_run(str(data[col][row+1]))
            cell_run_font(run2)


def set_cell_border(cell, **kwargs):
    """
    Customize the cell borders.
    refer: https://stackoverflow.com/a/49615968

    Usage:
    set_cell_border(
        cell,
        top={"sz": 12, "val": "single", "color": "#FF0000", "space": "0"},
        bottom={"sz": 12, "color": "#00FF00", "val": "single"},
        start={"sz": 24, "val": "dashed", "shadow": "true"},
        end={"sz": 12, "val": "dashed"},
    )
    """
    tc = cell._tc
    tcPr = tc.get_or_add_tcPr()

    # check for tag existence, if none found, then create one
    tcBorders = tcPr.first_child_found_in("w:tcBorders")
    if tcBorders is None:
        tcBorders = OxmlElement('w:tcBorders')
        tcPr.append(tcBorders)

    # list over all available tags
    for edge in ('start', 'top', 'end', 'bottom', 'insideH', 'insideV'):
        edge_data = kwargs.get(edge)
        if edge_data:
            tag = 'w:{}'.format(edge)

            # check for tag existence, if none found, then create one
            element = tcBorders.find(qn(tag))
            if element is None:
                element = OxmlElement(tag)
                tcBorders.append(element)

            # looks like order of attributes is important
            for key in ["sz", "val", "color", "space", "shadow"]:
                if key in edge_data:
                    element.set(qn('w:{}'.format(key)), str(edge_data[key]))


def add_fig_below_label(document_obj, label: str):
    """
    Function adds a label below the figures with red font color.
    paragraph style is set to "Body Text" which is crucial for
    a separate table of content of all the figures.

    :param document_obj: doc object
    :param label: TEXT
    :return:
    """
    para_below = document_obj.add_paragraph()
    para_below.style = document_obj.styles["Body Text"]
    para_below.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    below_label = para_below.add_run(label)
    font = below_label.font
    font.color.rgb = RGBColor(255, 0, 0)


def add_table_below_label(doc_obj, label: str):
    """
    Function adds a label below the table with red font color.
    paragraph style is set to "Body Text 2" which is crucial for
    a separate table of content of all the figures.
    :param doc_obj:  doc object
    :param label: TEXT
    :return:
    """
    para_below = doc_obj.add_paragraph()
    para_below.style = doc_obj.styles["Body Text 2"]
    para_below.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
    below_label = para_below.add_run(label)
    font = below_label.font
    font.color.rgb = RGBColor(255, 0, 0)


def change_orientation(doc_obj, landscape=None, portrait=None):
    """
    This function changes the orientation to the one mentioned in parameter.
    ## [TODO] 1. Takes the path [of the header and footer logos] needs to be dynamic in future. Right now hardcoded.
              2. The footer text is hardcoded needs to dynamic once we have the data.
              3. Maybe optimized ??
    Do not provide both values at same time.
    :param doc_obj: doc object
    :param landscape: generally landscape=1 means we need to change to the landscape [only for developer use]
    :param portrait: generally portrait=1 means we need to change to the portrait  [only for developer use]
    :return:
    """
    current_section = doc_obj.sections[-1]
    new_width, new_height = current_section.page_height, current_section.page_width
    new_section = doc_obj.add_section(WD_SECTION_START.NEW_PAGE)
    if landscape:
        # Defining Header
        land_header = new_section.header
        land_header.is_linked_to_previous = False
        add_header(new_section, landscape=True)

        # Defining Footer
        land_footer = new_section.footer
        land_footer.is_linked_to_previous = False
        land_footer_para = land_footer.paragraphs[0]

        # PAGE NUMBER
        add_page_number(land_footer_para.add_run())  # https://stackoverflow.com/a/56676220
        land_footer_para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

        add_float_picture(land_footer_para,
                          r"{}".format(get_correct_path('\\Resources\\header_footer_img\\footer_behind_trans.png')),
                          width=Cm(7.48), height=Cm(6.68), pos_x=Cm(-3.9), pos_y=Cm(17.73))
        add_float_picture(land_footer_para,
                          r"{}".format(get_correct_path('\\Resources\\header_footer_img\\footer_front_trans.png')),
                          width=Cm(4.06), height=Cm(4.93), pos_x=Cm(-0.14), pos_y=Cm(17.47))
        foot_table = land_footer.add_table(rows=1, cols=2, width=Cm(29.83))
        indent_table(foot_table, Pt(5))  # https://stackoverflow.com/a/55142896

        data = footer_data
        for row in range(1):
            for col in range(2):
                cell = foot_table.cell(row, col)
                cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
                para = cell.paragraphs[0]
                if row == 0 and col == 1:
                    cell.width = Cm(19.66)
                    para.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
                else:
                    cell.width = Cm(10.17)
                    para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
                run1 = para.add_run(data[col][row])
                font = run1.font
                font.bold = True
                font.color.rgb = RGBColor(255, 0, 0)
                para.add_run(data[col][row+1])

        new_section.orientation = WD_ORIENTATION.LANDSCAPE
        new_section.page_width = new_width
        new_section.page_height = new_height
        new_section.top_margin = Cm(0.6)
        new_section.bottom_margin = Cm(0.6)
        new_section.left_margin = Cm(2.54)
        new_section.right_margin = Cm(1.59)
        new_section.footer_distance = Cm(0)
        new_section.header_distance = Cm(1.8)
    elif portrait:
        # Defining Header
        por_header = new_section.header
        por_header.is_linked_to_previous = False
        add_header(new_section)
        # Defining Footer
        por_footer = new_section.footer
        por_footer.is_linked_to_previous = False
        add_footer(new_section)
        new_section.orientation = WD_ORIENTATION.PORTRAIT
        new_section.page_width = new_width
        new_section.page_height = new_height
        new_section.top_margin = Cm(2.25)
        new_section.bottom_margin = Cm(1.59)
        new_section.left_margin = Cm(2.54)
        new_section.right_margin = Cm(2.54)
        new_section.footer_distance = Cm(0)

        return new_section


def add_new_row(table_name):
    """
    ##[TODO] I think no need of this function but used in SECTION 3.0.
    :param table_name:
    :return:
    """
    return table_name.add_row()


def add_defect_colored(para, defect_type, color=False, color_name="", bold=False):
    """
    Used in Section 3.0 heading 3.x.4 Structural Assessment.
    Changes the the color of Major, Mod, Minor defects accordingly.
    :param para: paragraph
    :param defect_type: Needs input whether Maj, Mod, Min
    :param color: Want colored or not.
    :param color_name: Color name should be provided ##TODO Maybe we can add logic for it.
    :param bold: Want bold or not ?
    :return: returns the text run
    """
    if color:
        run = para.add_run(color_name)
    else:
        run = para.add_run(defect_type.title())
    font = run.font
    font.name = "Georgia"
    font.size = Pt(12)
    if bold:
        font.bold = True
    if defect_type.lower() == "major":
        font.color.rgb = RGBColor(255, 0, 0)

    elif defect_type.lower() == "moderate":
        font.color.rgb = RGBColor(237, 125, 49)

    elif defect_type.lower() == "minor":
        font.color.rgb = RGBColor(191, 143, 0)

    return run


def defect_table(doc_name):
    """
    Used in SECTION 3.0
    Creates the main defect table only header rows.
    ## [TODO] Maybe optimized in future.
    :param doc_name: doc object
    :return: table
    """
    defect_table_head = doc_name.add_table(rows=2, cols=5, style="ESA_table_style_main")
    defect_table_head.allow_autofit = False

    # Merging Cells
    merge_ver_cell(defect_table_head, 0)
    merge_ver_cell(defect_table_head, 1)
    merge_ver_cell(defect_table_head, 4)
    c0, c1 = defect_table_head.cell(0, 2), defect_table_head.cell(0, 3)
    c0.merge(c1)

    # Setting the width
    parameters = {"ID": 1.24, "Defect Type /\n Severity": 3.25, "Defect Details": 8, "Defect Detail": 8,
                  "Defect Snapshot": 13}
    for i in range(1):
        for j in range(5):
            cell = defect_table_head.cell(i, j)
            cell.width = Cm(parameters[list(parameters)[j]])
            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            if j == 3:
                continue
            run = para.add_run(list(parameters)[j])
            font = run.font
            font.name = "Georgia"
            font.bold = True
            font.size = Pt(10)

    data = ["Location &\nDepth (m)", "Dimension of Defects\n(mm) (L x W)"]
    data_i = 0

    for i in range(1, 2):
        for j in range(2, 4):
            cell = defect_table_head.cell(i, j)
            cell._tc.get_or_add_tcPr().append(parse_xml(r'<w:shd {} w:fill="538A86"/>'.format(nsdecls('w'))))
            cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            cell.width = Cm(3.25)
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            run = para.add_run(data[data_i])
            font = run.font
            font.name = "Georgia"
            font.bold = True
            font.size = Pt(10)
            font.color.rgb = RGBColor(255, 255, 255)
            data_i += 1
    return defect_table_head
