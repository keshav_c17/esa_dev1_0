#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# '''===================================================================================================================
# @Project : ESA    Filename: esa_section_1.py
# @Version : 1.0
# @Author  : Keshav Choudhary
# @Email   : keshav.c@planystech.com
# '''===================================================================================================================
""" This module creates the section 1 of the report."""
# '''===================================================================================================================
import os.path

from Utilities.esa_utils import add_section_heading, heading_font_change, merge_hor_cell, set_cell_border, check_excel_file_path
from Utilities.esa_image_utils import get_graph_paths, get_4_major_defects, icons_path
from Utilities.opensource_scripts.floating_picture import add_float_picture
from docx.shared import RGBColor, Pt, Cm
from docx.enum.table import WD_TABLE_ALIGNMENT, WD_CELL_VERTICAL_ALIGNMENT
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT
import pandas as pd
import sys

excel_path = sys.argv[1]+"\\files\\Resources\\Automation_template.xlsx"
check_excel_file_path()


def section_1_heading(doc_obj):
    """
    Creates the top heading with red line.
    """
    para = add_section_heading(doc_obj, "1.0 Executive Summary")
    heading_font_change(para, "Georgia", Pt(20), True, 0, 0, 0)


def creating_top_table(doc_obj):
    """
    1. Creates 4x4 table and removes the border as in the original report and colors
        the border of cell to red.
    2. Then merge the required cells
    3. Then inserts the icon to specific cells.
    4. Finally fills the data in each cell plus set width of cell and row height.
    ## [TODO] 1. Data completely static, needs to make it dynamic once we get the data.
              2. Icon path needs to be dynamic when we move it to other systems.
    """
    top_table = doc_obj.add_table(rows=4, cols=4)
    top_table.style = "Table Grid"
    top_table.autofit = False
    top_table.allow_autofit = False
    top_table.width = Cm(16.94)

    formatting_top_table(top_table)

    insert_icons(top_table)

    populate_top_table(top_table)


def formatting_top_table(table):
    """
    COLORING TABLE RED AND REMOVING OUTSIDE BORDERS also MERGING THE CELLS
    """
    for i in range(4):
        for j in range(4):
            cell = table.cell(i, j)
            set_cell_border(cell, top={"sz": 10, "val": "single", "color": "#FF0000"},
                            bottom={"sz": 10, "color": "#FF0000", "val": "single"},
                            start={"sz": 10, "color": "#FF0000", "val": "single"},
                            end={"sz": 10, "color": "#FF0000", "val": "single"})
            if i == 0:
                set_cell_border(cell, top={"val": "nil"})
            if i == 3:
                set_cell_border(cell, bottom={"val": "nil"})
            if j == 0:
                set_cell_border(cell, start={"val": "nil"})
            if j == 3 or (i <= 1 or j == 1):
                set_cell_border(cell, end={"val": "nil"})
    # Merging required cells
    merge_hor_cell(table, 0)
    merge_hor_cell(table, 1)


def insert_icons(table):
    # INSERTING ICONS
    i = 0
    paths = icons_path()[0]
    for path in paths:
        cell = table.cell(i, 0)
        para = cell.paragraphs[0]
        if i == 2:
            para.add_run("            ")
        if i == 3:
            para.add_run("         ")
        add_float_picture(para, r"{}".format(path.rstrip()), pos_x=Cm(0.2), pos_y=Cm(0))
        i += 1
        if i == 4:
            break
    # INSERTING ICONS MANUALLY
    calendar_cell = table.cell(2, 2)
    para = calendar_cell.paragraphs[0]
    para.add_run("        ")
    add_float_picture(para, paths[4], pos_x=Cm(0.15), pos_y=Cm(-0.09))

    underwater_cell = table.cell(3, 2)
    para = underwater_cell.paragraphs[0]
    para.add_run("     ")
    add_float_picture(para, paths[-1], pos_x=Cm(0.1), pos_y=Cm(0.05))


def populate_top_table(table):
    """Populating the top table"""

    df = pd.read_excel(excel_path, sheet_name="executive_summary").dropna()
    data = [
        ["Area of Interest", df.iloc[0]["Area of Interest"]],
        ["Inspection Method", df.iloc[0]["Inspection Method"]],
        ["Equipment Used", f"{df.iloc[0]['Equipment Used']} (Refer Appendix 4.2)", "Date/Duration", df.iloc[0]['Date']],
        ["Crew Size", f"{df.iloc[0]['Crew Size']} (Refer Appendix 4.3)", "Underwater Visibility",
         df.iloc[0]["Underwater Visibility"]]
    ]
    # POPULATING TOP 2 ROWS
    for i in range(2):
        for j in range(2):

            if (i == 0 and j == 1) or (i == 1 and j == 1):
                cell = table.cell(i, j + 1)
                table.cell(i, j + 1).vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            else:
                cell = table.cell(i, j)
                table.cell(i, j).vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
                cell.width = Cm(8)
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            para.add_run(str(data[i][j]))

    # POPULATING BOTTOM 2 ROWS
    for i in range(2, 4):
        for j in range(4):
            table.cell(i, j).vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            cell = table.cell(i, j)
            cell.width = Cm(4.3)
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            para.add_run(str(data[i][j]))

    # TOP TABLE DIMENSIONS
    for row in table.rows[:2]:
        row.height = Cm(1.49)
    for row in table.rows[2:]:
        row.height = Cm(1.44)


def adding_graph_images(doc_obj):
    """
    1. Adding the graph picture in a single cell table one below the other.

    ## [TODO] 1. All the graph paths and Path to the paths.txt file need to be generated dynamically.
    """
    # ADDING EMPTY LINE BEFORE IMAGES
    doc_obj.add_paragraph()
    # ADDING GRAPH PICTURES
    for path in get_graph_paths()[:-1]:
        graph_table = doc_obj.add_table(rows=1, cols=1)
        graph_cell = graph_table.cell(0, 0)
        graph_cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
        para = graph_cell.paragraphs[0]
        para.add_run().add_picture(r"{}".format(path.rstrip()), width=Cm(16.09), height=Cm(5.2))


def create_align_table(doc_obj):
    """
    This is a 1x2 table in which 1st cell has pie graph and 2nd cell has defects category table.
    It is implemented this way because it aligns both pie graph and def_cat_table side by side.
    """
    align_table = doc_obj.add_table(rows=1, cols=2)
    row = align_table.rows[0]
    row.height = Cm(5.54)

    add_pie_graph(align_table)

    add_defect_cat_table(align_table)


def add_pie_graph(table):
    """
    ADDING PIE GRAPH & DEFECT TYPE TABLE SIDE BY SIDE
    """
    cell1 = table.cell(0, 0)
    cell1.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.TOP
    para = cell1.paragraphs[0]
    para.add_run().add_picture(get_graph_paths()[-1], width=Cm(8.5725), height=Cm(4.8683))


def add_defect_cat_table(table):
    """
    ADDING TABLE IN TABLE [DEF_TABLE]
    """
    cell2 = table.cell(0, 1)
    cell2.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.TOP
    # deleting empty paragraph of cell
    para = cell2.paragraphs[0]
    p = para._element
    p.getparent().remove(p)
    p._p = p._element = None

    # creating table
    def_type_table = cell2.add_table(4, 2)
    def_type_table.style = "Table Grid"
    def_type_table.alignment = WD_TABLE_ALIGNMENT.RIGHT
    def_type_table.allow_autofit = False

    # DEFECTS COLOR ICON [LOOP POSSIBLE, X-Y ARE SAME FOR EACH ICON]
    paths = icons_path()[1]
    i = 1
    for path in paths:
        cell = def_type_table.cell(i, 0)
        para = cell.paragraphs[0]
        if i == 2:
            para.add_run("      ")
        add_float_picture(para, r"{}".format(path.rstrip()), pos_x=Cm(0.1), pos_y=Cm(0.03))
        i += 1

    # POPULATING defects type table
    data = [
        ["Category of Defect", "Type of Defects Observed"],
        ["Major", "Surface Deformation"],
        ["Moderate", "Surface Deformation, Cavity"],
        ["Minor", "Cavity"]
    ]
    for row in range(4):
        for col in range(2):
            def_type_table.cell(row, col).vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
            cell = def_type_table.cell(row, col)
            set_cell_border(cell, top={"sz": 10, "val": "single", "color": "#FF0000"},
                            bottom={"sz": 10, "color": "#FF0000", "val": "single"},
                            start={"sz": 10, "color": "#FF0000", "val": "single"},
                            end={"sz": 10, "color": "#FF0000", "val": "single"})
            cell.width = Cm(9.39)
            row_selected = def_type_table.rows[row]
            row_selected.height = Cm(1.08)
            para = cell.paragraphs[0]
            para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
            if row == 0 and (col == 0 or col == 1):
                run = para.add_run(data[row][col])
                font = run.font
                font.color.rgb = RGBColor(255, 0, 0)
            else:
                para.add_run(data[row][col])


def sec_1_last_table(doc_obj):
    """
    1. Creates the last table of Section 1 containing 4 [maybe] random defects.
    2. Sets all the dimension of the cells also colors the cell border to red.
    3. Along side it all also fills the data into the cell all in single loop.
    ## [TODO]: 1. All the graph paths need to be generated dynamically.
               2. Possibility of table to divide onto two pages if page-break not provided.
    :param doc_obj:
    :return:
    """

    # Final Defects Image Table
    defects_img_table = doc_obj.add_table(rows=2, cols=2)
    defects_img_table.alignment = WD_TABLE_ALIGNMENT.CENTER
    defects_img_table.style = "Table Grid"

    row, col = 0, 0
    for path in get_4_major_defects():
        cell = defects_img_table.cell(row, col)
        cell.width = Cm(6.32)
        set_cell_border(cell, top={"sz": 10, "val": "double", "color": "#FF0000"},
                        bottom={"sz": 10, "color": "#FF0000", "val": "double"},
                        start={"sz": 10, "color": "#FF0000", "val": "double"},
                        end={"sz": 10, "color": "#FF0000", "val": "double"})
        if col == 0:
            set_cell_border(cell, start={"sz": 10, "color": "#FF0000", "val": "nil"})
        else:
            set_cell_border(cell, end={"sz": 10, "color": "#FF0000", "val": "nil"})
        if row == 0 and col <= 1:
            set_cell_border(cell, top={"sz": 10, "val": "nil", "color": "#FF0000"})
        elif row == 1 and col <= 1:
            set_cell_border(cell, bottom={"sz": 10, "val": "nil", "color": "#FF0000"})
        cell.vertical_alignment = WD_CELL_VERTICAL_ALIGNMENT.CENTER
        para = cell.paragraphs[0]
        para.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
        para.add_run().add_picture(r"{}".format(path.rstrip()), width=Cm(5.22), height=Cm(2.94))
        if row == 0 and col == 1:
            row += 1
            col -= 1
        else:
            col += 1

    total_graphs = len(get_graph_paths()) - 1
    if not total_graphs == 1:
        doc_obj.add_page_break()
